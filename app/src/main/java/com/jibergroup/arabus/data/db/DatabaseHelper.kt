package com.jibergroup.arabus.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jibergroup.arabus.data.db.entities.*
import com.jibergroup.arabus.utils.DateConverter

const val DATABASE_NAME = "arabus.sqlite"

@Database(
    entities = [KazakhWord::class, KazakhGames::class, RussianWord::class, RussianGames::class],
    version = 3,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class DatabaseHelper : RoomDatabase() {

    abstract fun kazakhWordsDao(): WordsDao
    abstract fun russianWordsDao(): RussianWordsDao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseHelper? = null

        fun getInstance(context: Context): DatabaseHelper {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DatabaseHelper::class.java,
                DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .createFromAsset(DATABASE_NAME)
                .build()
    }
}