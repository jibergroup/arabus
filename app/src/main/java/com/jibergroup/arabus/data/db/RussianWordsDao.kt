package com.jibergroup.arabus.data.db

import androidx.room.Dao
import androidx.room.Query
import com.jibergroup.arabus.data.db.entities.RussianWord
import com.jibergroup.arabus.ui.training_words.GameDataRussian
import java.util.*

@Dao
interface RussianWordsDao {

    @Query("select  * from russian where arabic = :arabicSimilarWord limit 100")
    suspend fun loadRussianWordsWithArabic(arabicSimilarWord: String): List<RussianWord>

    @Query("select distinct * from (select *, 1 as order_prefix from russian k where k.search =:arabicSimilarWord union all select *, 2 as order_prefix from russian k where k.plural_letters = :arabicSimilarWord) k order by k.order_prefix limit 100")
    suspend fun loadRussianWordsWithArabic2(arabicSimilarWord: String?): List<RussianWord>

    @Query("select distinct * from (select *, 1 as order_prefix from russian k where lower(k.russian)=:word union all select *, 2 as order_prefix from russian k where k.short_words like '% ' || :word || ' %' union all select *, 3 as order_prefix from russian k where long_words like '%' || :word || '%') k order by k.order_prefix limit 100")
    suspend fun loadRussianWordsWithMain(word: String): List<RussianWord>

    @Query("select * from russian where lower(russian) like '%' || :descriptionSimilarWord || '%' limit 100")
    suspend fun loadRussianWordsWithRussian(descriptionSimilarWord: String): List<RussianWord>

    @Query("select * from russian where lower(description) like '%' || :descriptionSimilarWord || '%' limit 100")
    suspend fun loadRussianWordsWithDescription(descriptionSimilarWord: String): List<RussianWord>

    @Query("SELECT * FROM russian WHERE root_id = :root AND id != :myId")
    suspend fun loadRussianRootWords(root: String, myId: Int): List<RussianWord>

    @Query("SELECT * FROM russian WHERE is_chosen = 1")
    suspend fun loadRussianWordFavorites(): List<RussianWord>

    @Query("SELECT * FROM russian WHERE viewed_at IS NOT NULL ORDER BY viewed_at DESC LIMIT 10")
    suspend fun loadRussianWordHistory(): List<RussianWord>

    @Query("UPDATE russian SET is_chosen = :isFavorite WHERE id = :wordId")
    suspend fun updateRussianWordFavorites(wordId: Int, isFavorite: Boolean) : Int

    @Query("UPDATE russian SET viewed_at = :viewedAt WHERE id = :wordId")
    suspend fun updateRussianWordHistory(wordId: Int, viewedAt: Date)

    @Query("UPDATE russian SET viewed_at = :viewedAt")
    suspend fun deleteAllHistories(viewedAt: Date?)

    @Query("INSERT INTO russian_games (word_id) VALUES(:wordId)")
    suspend fun addRussianWordSprint(wordId: Int)

    @Query("DELETE FROM russian_games WHERE word_id = :wordId")
    suspend fun deleteRussianWordSprint(wordId: Int)

    @Query("DELETE FROM russian_games WHERE word_id in(:ids)")
    suspend fun deleteRussianGameTableIds(ids : List<Int?>)

    @Query("UPDATE russian SET is_in_game = 0 WHERE id in(:ids)")
    suspend fun updateRussianWordsIsInGameState(ids : List<Int?>)

    @Query("UPDATE russian SET is_in_game = :state WHERE id = :wordId")
    suspend fun updateRussianWordIsInGameState(wordId: Int, state: Boolean)

    @Query("SELECT * FROM russian WHERE is_in_game = 1")
    suspend fun loadTrainRussianWords() : List<GameDataRussian>

    @Query("UPDATE russian_games SET constructor = :state WHERE word_id = :wordId")
    suspend fun updateGameModeConstructor(wordId: Int, state: Boolean)

    @Query("UPDATE russian_games SET sprint = :state WHERE word_id = :wordId")
    suspend fun updateGameModeSprint(wordId: Int, state: Boolean)

    @Query("UPDATE russian_games SET arabic_game = :state WHERE word_id = :wordId")
    suspend fun updateGameModeArabic(wordId: Int, state: Boolean)

    @Query("UPDATE russian_games SET foreign_game = :state WHERE word_id = :wordId")
    suspend fun updateGameModeForeign(wordId: Int, state: Boolean)

    @Query("UPDATE russian_games SET foreign_game = :state, arabic_game = :state, constructor = :state, sprint = :state WHERE word_id = :wordId")
    suspend fun updateGameModeAll(wordId: Int, state: Boolean)

    @Query("SELECT * FROM russian WHERE russian.russian NOT NULL ORDER BY RANDOM() LIMIT 1000")
    suspend fun loadRandomWords() : List<RussianWord>

}