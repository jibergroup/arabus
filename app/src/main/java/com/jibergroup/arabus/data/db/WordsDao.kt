package com.jibergroup.arabus.data.db

import androidx.room.Dao
import androidx.room.Query
import com.jibergroup.arabus.data.db.entities.KazakhGames
import com.jibergroup.arabus.data.db.entities.KazakhWord
import com.jibergroup.arabus.ui.training_words.GameData
import com.jibergroup.arabus.ui.training_words.GameDataKazakh
import java.util.*

@Dao
interface WordsDao {

    @Query("select  * from kazakh where arabic = :arabicSimilarWord limit 100")
    suspend fun loadKazakhWordsWithArabic(arabicSimilarWord: String): List<KazakhWord>

    @Query("select distinct * from (select *, 1 as order_prefix from kazakh k where k.search =:arabicSimilarWord union all select *, 2 as order_prefix from kazakh k where k.plural_letters = :arabicSimilarWord) k order by k.order_prefix limit 100")
    suspend fun loadKazakhWordsWithArabic2(arabicSimilarWord: String?): List<KazakhWord>

    @Query("select distinct * from (select *, 1 as order_prefix from kazakh k where lower(k.kazakh)=:kazakhSimilarWord union all select *, 2 as order_prefix from kazakh k where k.short_words like '% ' || :kazakhSimilarWord || ' %' union all select *, 3 as order_prefix from kazakh k where long_words like '%' || :kazakhSimilarWord || '%') k order by k.order_prefix limit 100")
    suspend fun loadKazakhWordsWithMain(kazakhSimilarWord: String): List<KazakhWord>

    @Query("select * from kazakh where lower(kazakh) like '%' || :descriptionSimilarWord || '%' limit 100")
    suspend fun loadKazakhWordsWithKazakh(descriptionSimilarWord: String): List<KazakhWord>

    @Query("select * from kazakh where lower(description) like '%' || :descriptionSimilarWord || '%' limit 100")
    suspend fun loadKazakhWordsWithDescription(descriptionSimilarWord: String): List<KazakhWord>

    @Query("SELECT * FROM kazakh WHERE root_id = :root AND id != :myId ORDER BY root_id")
    suspend fun loadKazakhRootWords(root: String, myId: Int): List<KazakhWord>

    @Query("SELECT * FROM kazakh WHERE is_chosen = 1")
    suspend fun loadKazakhWordFavorites(): List<KazakhWord>

    @Query("SELECT * FROM kazakh WHERE viewed_at IS NOT NULL ORDER BY viewed_at DESC LIMIT 10")
    suspend fun loadKazakhWordHistory(): List<KazakhWord>

    @Query("UPDATE kazakh SET is_chosen = :isFavorite WHERE id = :wordId")
    suspend fun updateKazakhWordFavorites(wordId: Int, isFavorite: Boolean) : Int

    @Query("UPDATE kazakh SET viewed_at = :viewedAt WHERE id = :wordId")
    suspend fun updateKazakhWordHistory(wordId: Int, viewedAt: Date)

    @Query("UPDATE kazakh SET viewed_at = :viewedAt")
    suspend fun deleteAllHistories(viewedAt: Date?)

    @Query("INSERT INTO kazakh_games (word_id) VALUES(:wordId)")
    suspend fun addKazakhWordSprint(wordId: Int)

    @Query("DELETE FROM kazakh_games WHERE word_id = :wordId")
    suspend fun deleteKazakhWordSprint(wordId: Int)

    @Query("DELETE FROM kazakh_games WHERE word_id in(:ids)")
    suspend fun deleteKazakhGameTableIds(ids : List<Int?>)

    @Query("UPDATE kazakh SET is_in_game = 0 WHERE id in(:ids)")
    suspend fun updateKazakhWordsIsInGameState(ids : List<Int?>)

    @Query("UPDATE kazakh SET is_in_game = :state WHERE id = :wordId")
    suspend fun updateKazakhWordIsInGameState(wordId: Int, state: Boolean)

    @Query("SELECT * FROM kazakh WHERE is_in_game = 1")
    suspend fun loadTrainKazakhWords() : List<GameDataKazakh>

    @Query("UPDATE kazakh_games SET constructor = :state WHERE word_id = :wordId")
    suspend fun updateGameModeConstructor(wordId: Int, state: Boolean)

    @Query("UPDATE kazakh_games SET sprint = :state WHERE word_id = :wordId")
    suspend fun updateGameModeSprint(wordId: Int, state: Boolean)

    @Query("UPDATE kazakh_games SET arabic_game = :state WHERE word_id = :wordId")
    suspend fun updateGameModeArabic(wordId: Int, state: Boolean)

    @Query("UPDATE kazakh_games SET foreign_game = :state WHERE word_id = :wordId")
    suspend fun updateGameModeForeign(wordId: Int, state: Boolean)

    @Query("UPDATE kazakh_games SET foreign_game = :state,arabic_game = :state, constructor = :state, sprint = :state WHERE word_id = :wordId")
    suspend fun updateGameModeAll(wordId: Int, state: Boolean)

    @Query("SELECT * FROM kazakh WHERE kazakh.kazakh NOT NULL ORDER BY RANDOM() LIMIT 1000")
    suspend fun loadRandomWords() : List<KazakhWord>
}