package com.jibergroup.arabus.data.db.entities

import java.io.Serializable
import java.util.*


data class Word(
    val id: Int,
    val text: String,
    val description: String,
    val arabic: String,
    val arabicSecond: String,
    val root: String,
    var isFavorite: Boolean,
    var isInGame: Boolean,
    var viewedAt: Date
) : Serializable
