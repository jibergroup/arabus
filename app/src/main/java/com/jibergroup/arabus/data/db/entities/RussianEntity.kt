package com.jibergroup.arabus.data.db.entities

import androidx.room.*
import com.jibergroup.arabus.utils.DateConverter
import java.util.*

@Entity(tableName = "russian")
data class RussianWord(
    @PrimaryKey
    val id: Int?,
    val search: String?,
    val russian: String?,
    val arabic: String?,
    @ColumnInfo(name = "arabic_sec")
    val arabicSecond: String?,
    @ColumnInfo(name = "plural_letters")
    val pluralLetters: String?,
    @ColumnInfo(name = "long_words")
    val longWords: String?,
    @ColumnInfo(name = "short_words")
    val shortWords: String?,
    val description: String?,
    @ColumnInfo(name = "root_id")
    val rootId: String?,
    @ColumnInfo(name = "is_chosen", defaultValue = "0")
    var isChosen: Boolean?,
    @ColumnInfo(name = "is_in_game", defaultValue = "0")
    var isInGame: Boolean?,
    @TypeConverters(DateConverter::class)
    @ColumnInfo(name = "viewed_at")
    var viewedAt: Date? = null
)

@Entity(tableName = "russian_games")
data class RussianGames(
    @PrimaryKey
    val id: Int,
    val word_id: Int,
    @ColumnInfo(name = "constructor", defaultValue = "1")
    var constructor: Boolean?,
    @ColumnInfo(name = "sprint", defaultValue = "1")
    var sprint: Boolean?,
    @ColumnInfo(name = "arabic_game", defaultValue = "1")
    var arabicGame: Boolean?,
    @ColumnInfo(name = "foreign_game", defaultValue = "1")
    var foreignGame: Boolean?
)