package com.jibergroup.arabus.data.mapper

import com.jibergroup.arabus.data.db.entities.KazakhWord
import com.jibergroup.arabus.data.db.entities.Word
import java.util.*

class KazakhModelMapperWord : BaseMapper<KazakhWord, Word>{

    override fun transform(type: KazakhWord): Word =
        Word(
            id = type.id ?:0,
            text = type.kazakh ?: "",
            description = type.description ?: "",
            arabic = type.arabic ?: "",
            arabicSecond = type.arabicSecond ?: "",
            root = type.rootId ?: "",
            isInGame = type.isInGame ?: false,
            isFavorite = type.isChosen ?: false,
            viewedAt = type.viewedAt ?: Date()
        )

    override fun transformToDatabase(type: Word): KazakhWord =
        KazakhWord(
            id = type.id,
            search = "",
            kazakh = type.text,
            arabic = type.arabic,
            arabicSecond = type.arabicSecond,
            pluralLetters = "",
            rootId = type.root,
            isChosen = false,
            isInGame = false,
            description = "",
            shortWords = "",
            longWords = ""
        )
}