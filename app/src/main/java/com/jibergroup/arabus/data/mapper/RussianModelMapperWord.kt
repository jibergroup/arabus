package com.jibergroup.arabus.data.mapper

import com.jibergroup.arabus.data.db.entities.RussianWord
import com.jibergroup.arabus.data.db.entities.Word
import java.util.*

class RussianModelMapperWord : BaseMapper<RussianWord, Word> {

    override fun transform(type: RussianWord): Word {
        return Word(
            id = type.id ?:0,
            text = type.russian ?: "",
            description = type.description ?: "",
            arabic = type.arabic ?: "",
            arabicSecond = type.arabicSecond ?: "",
            root = type.rootId ?: "",
            isInGame = type.isInGame ?: false,
            isFavorite = type.isChosen ?: false,
            viewedAt = type.viewedAt ?: Date()
        )
    }

    override fun transformToDatabase(type: Word): RussianWord {
        return RussianWord(
            id = type.id,
            search = "",
            russian = type.text,
            arabic = type.arabic,
            arabicSecond = type.arabicSecond,
            pluralLetters = "",
            rootId = type.root,
            isChosen = false,
            isInGame = false,
            description = type.description,
            shortWords = "",
            longWords = ""
        )
    }

}