package com.jibergroup.arabus.data.pref

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jibergroup.arabus.utils.getKoinInstance
import java.lang.reflect.Type


const val PREF_NAME = "PREF_NAME"
const val PREF_KEY_LOCALE = "PREF_KEY_LOCALE"
const val PREF_KEY_LIMIT_WORD = "PREF_KEY_LIMIT_WORD"
const val PREF_KEY_APP = "PREF_KEY_APP"
const val APP_BASE = 1
const val APP_FULL = 2
const val LIMIT = 20

object PreferenceHelper{

    private val context : Context = getKoinInstance()

    private val mPrefs: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    fun setLocaleLang(lang : String){
        mPrefs.edit().putString(PREF_KEY_LOCALE, lang).apply()
    }

    fun getLocaleLang(): String{
        return mPrefs.getString(PREF_KEY_LOCALE,"kk") ?: "kk"
    }

    fun localeLangIsKazakh(): Boolean{
        return mPrefs.getString(PREF_KEY_LOCALE,"kk").equals("kk")
    }

    fun addWord(wordId: Int){
        val gson = Gson()
        val textList: HashSet<Int> = getWords()
        textList.add(wordId)
        val jsonText = gson.toJson(textList)
        mPrefs.edit().putString(PREF_KEY_LIMIT_WORD, jsonText).apply()
    }

    fun removeWord(wordId: Int){
        val gson = Gson()
        val textList: HashSet<Int> = getWords()
        textList.remove(wordId)
        val jsonText = gson.toJson(textList)
        mPrefs.edit().putString(PREF_KEY_LIMIT_WORD, jsonText).apply()
    }

    fun clear(){
        mPrefs.edit().remove(PREF_KEY_LIMIT_WORD).apply()
    }

    private fun getWords(): HashSet<Int>{
        val gson = Gson()
        val json: String = mPrefs.getString(PREF_KEY_LIMIT_WORD, null) ?: return HashSet()
        val type: Type = object : TypeToken<Set<Int>>() {}.type
        val list: HashSet<Int> = gson.fromJson(json, type)
        if(list.isNullOrEmpty()){
            return HashSet()
        }
        return list
    }

    fun checkAccess(state: Boolean): Boolean{
        return if(state){
            getWords().size < LIMIT
        } else{
            true
        }
    }

    fun getAppAccessibility() : Int{
        return mPrefs.getInt(PREF_KEY_APP, APP_BASE)
    }

    fun setAppAccessibility(count : Int){
        mPrefs.edit().putInt(PREF_KEY_APP,count).apply()
    }

}