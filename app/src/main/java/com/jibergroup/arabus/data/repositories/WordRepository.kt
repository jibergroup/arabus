package com.jibergroup.arabus.data.repositories

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.jibergroup.arabus.data.db.RussianWordsDao
import com.jibergroup.arabus.data.db.WordsDao
import com.jibergroup.arabus.data.db.entities.KazakhWord
import com.jibergroup.arabus.data.db.entities.RussianWord
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.mapper.KazakhModelMapperWord
import com.jibergroup.arabus.data.mapper.RussianModelMapperWord
import com.jibergroup.arabus.data.pref.PreferenceHelper
import com.jibergroup.arabus.ui.training_words.GameData
import com.jibergroup.arabus.ui.training_words.GameModelMapperKazakh
import com.jibergroup.arabus.ui.training_words.GameModelMapperRussian
import com.jibergroup.arabus.utils.Constants
import java.util.*

class WordRepository(
    private val wordsDao: WordsDao,
    private val russianWordsDao: RussianWordsDao
) {
    suspend fun loadWords(similarWord: String, isArabic: Boolean): List<Word> {
        return if(PreferenceHelper.localeLangIsKazakh()){
            val mapperLocal = KazakhModelMapperWord()
            loadKazakhWords(similarWord, isArabic).map { mapperLocal.transform(it) }
        }
        else{
            val mapperLocal = RussianModelMapperWord()
            loadRussianWords(similarWord, isArabic).map { mapperLocal.transform(it) }
        }
    }

    private suspend fun loadKazakhWords(similarWord: String, isArabic: Boolean) : List<KazakhWord>{
        var list : List<KazakhWord>
        if(isArabic){
            list = wordsDao.loadKazakhWordsWithArabic(similarWord)
            if (list.isNullOrEmpty()){
                val text = getArabicLetters(similarWord)
                val text2 = replaceArabicLettersToEasyArabicLetters(text)
                list = wordsDao.loadKazakhWordsWithArabic2(text2)
            }
        }
        else{
            list = wordsDao.loadKazakhWordsWithMain(similarWord)
            if(list.isNullOrEmpty()){
                list = wordsDao.loadKazakhWordsWithKazakh(similarWord)
                if(list.isNullOrEmpty()){
                    list = wordsDao.loadKazakhWordsWithDescription(similarWord)
                }
            }
        }
        return list
    }

    private suspend fun loadRussianWords(similarWord: String, isArabic: Boolean) : List<RussianWord>{
        var list : List<RussianWord>
        if(isArabic){
            list = russianWordsDao.loadRussianWordsWithArabic(similarWord)
            if (list.isNullOrEmpty()){
                val text = getArabicLetters(similarWord)
                val text2 = replaceArabicLettersToEasyArabicLetters(text)
                list = russianWordsDao.loadRussianWordsWithArabic2(text2)
            }
        }
        else{
            list = russianWordsDao.loadRussianWordsWithMain(similarWord)
            if(list.isNullOrEmpty()){
                list = russianWordsDao.loadRussianWordsWithRussian(similarWord)
                if(list.isNullOrEmpty()){
                    list = russianWordsDao.loadRussianWordsWithDescription(similarWord)
                }
            }
        }
        return list
    }

    suspend fun loadWordHistories():  List<Word>{
        return if(PreferenceHelper.localeLangIsKazakh()){
            val mapperLocal = KazakhModelMapperWord()
            wordsDao.loadKazakhWordHistory().map { mapperLocal.transform(it) }
        } else{
            val mapperLocal = RussianModelMapperWord()
            russianWordsDao.loadRussianWordHistory().map { mapperLocal.transform(it) }
        }
    }

    suspend fun loadRootWords(root: String, wordId: Int): List<Word>{
        return if (PreferenceHelper.localeLangIsKazakh()){
            val mapperLocal = KazakhModelMapperWord()
            wordsDao.loadKazakhRootWords(root, wordId).map { mapperLocal.transform(it) }
        }
        else{
            val mapperLocal = RussianModelMapperWord()
            russianWordsDao.loadRussianRootWords(root, wordId).map { mapperLocal.transform(it) }
        }
    }

    suspend fun onLoadFavoriteWords(): List<Word>{
        return if(PreferenceHelper.localeLangIsKazakh()){
            val mapperLocal = KazakhModelMapperWord()
            wordsDao.loadKazakhWordFavorites().map { mapperLocal.transform(it) }
        } else{
            val mapperLocal = RussianModelMapperWord()
            russianWordsDao.loadRussianWordFavorites().map { mapperLocal.transform(it) }
        }
    }

    suspend fun onUpdateWordFavorite(word: Word) : Boolean{
        return if(PreferenceHelper.localeLangIsKazakh()){
            wordsDao.updateKazakhWordFavorites(word.id, word.isFavorite) == 1
        }
        else{
            russianWordsDao.updateRussianWordFavorites(word.id, word.isFavorite) == 1
        }
    }

    suspend fun onUpdateWordHistory(wordId: Int, viewedAt: Date){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateKazakhWordHistory(wordId, viewedAt)
        }
        else{
            russianWordsDao.updateRussianWordHistory(wordId, viewedAt)
        }
    }

    suspend fun onLoadTrainingWords(): List<GameData>{
        return if(PreferenceHelper.localeLangIsKazakh()) {
            val mapperLocal = GameModelMapperKazakh()
            wordsDao.loadTrainKazakhWords().map { mapperLocal.transform(it) }

        }else{
            val mapperLocal = GameModelMapperRussian()
            russianWordsDao.loadTrainRussianWords().map { mapperLocal.transform(it) }
        }

    }

    suspend fun updateStateToSprint(wordId: Int, state : Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateKazakhWordIsInGameState(wordId = wordId,state = state)
            if(state){
                PreferenceHelper.addWord(wordId)
                wordsDao.addKazakhWordSprint(wordId = wordId)
            }else{
                PreferenceHelper.removeWord(wordId)
                wordsDao.deleteKazakhWordSprint(wordId = wordId)
            }
        }else{
            russianWordsDao.updateRussianWordIsInGameState(wordId = wordId,state = state)
            if(state){
                PreferenceHelper.addWord(wordId)
                russianWordsDao.addRussianWordSprint(wordId = wordId)
            }else{
                PreferenceHelper.removeWord(wordId)
                russianWordsDao.deleteRussianWordSprint(wordId = wordId)
            }
        }
    }

    suspend fun clearGameWords(data : List<GameData>){
        val ids : MutableList<Int?> = data.map { it.id }.toMutableList()
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateKazakhWordsIsInGameState(ids)
            wordsDao.deleteKazakhGameTableIds(ids)
        }else{
            russianWordsDao.updateRussianWordsIsInGameState(ids)
            russianWordsDao.deleteRussianGameTableIds(ids)
        }
        PreferenceHelper.clear()
    }


    suspend fun updateGameModeAll(wordId: Int, state: Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateGameModeAll(wordId, state)
        }else{
            russianWordsDao.updateGameModeAll(wordId, state)
        }
    }

    suspend fun updateGameModeConstructor(wordId: Int,state: Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateGameModeConstructor(wordId, state)
        }else{
            russianWordsDao.updateGameModeConstructor(wordId, state)
        }
    }

    suspend fun updateGameModeSprint(wordId: Int, state: Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateGameModeSprint(wordId, state)
        }else{
            russianWordsDao.updateGameModeSprint(wordId, state)
        }
    }

    suspend fun updateGameModeArabic(wordId: Int, state: Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateGameModeArabic(wordId, state)
        }else{
            russianWordsDao.updateGameModeArabic(wordId, state)
        }
    }

    suspend fun updateGameModeForeign(wordId: Int, state: Boolean){
        if(PreferenceHelper.localeLangIsKazakh()) {
            wordsDao.updateGameModeForeign(wordId, state)
        }else{
            russianWordsDao.updateGameModeForeign(wordId, state)
        }
    }

    suspend fun loadRandomAnswer() : List<Word>{
        return if(PreferenceHelper.localeLangIsKazakh()) {
            val mapperLocal = KazakhModelMapperWord()
            wordsDao.loadRandomWords().map { mapperLocal.transform(it) }
        }else{
            val mapperLocal = RussianModelMapperWord()
            russianWordsDao.loadRandomWords().map { mapperLocal.transform(it) }
        }
    }

    suspend fun deleteHistories() : Boolean{
        if(PreferenceHelper.localeLangIsKazakh()){
            wordsDao.deleteAllHistories(null)
        }
        else{
            russianWordsDao.deleteAllHistories(null)
        }
        return true
    }

    fun getAppAccessibility() : Int{
        return PreferenceHelper.getAppAccessibility()
    }

    fun setAppAccessibility(app: Int){
        PreferenceHelper.setAppAccessibility(app)
    }

    @Nullable
    fun getArabicLetters(text: String): String? {
        return deleteNotAvailableLetters(Constants.ARABIC_LETTERS, text)
    }

    @Nullable
    fun deleteNotAvailableLetters(@NonNull availableLetters: String, @NonNull text: String): String? {
        val sb = StringBuilder()
        for (ch in text.toCharArray()) {
            if (availableLetters.indexOf(ch) != -1) {
                sb.append(ch)
            }
        }
        return sb.toString()
    }

    private fun replaceArabicLettersToEasyArabicLetters(text: String?): String? {
        val sb = java.lang.StringBuilder()
        text?.let {
            for (ch in it.toCharArray()) {
                var ch2: Char
                val i = Constants.ARABIC_LETTERS.indexOf(ch)
                if (i != -1) {
                    ch2 = Constants.EASY_ARABIC_LETTERS[i]
                    sb.append(ch2)
                }
            }
        }
        return sb.toString()
    }
}