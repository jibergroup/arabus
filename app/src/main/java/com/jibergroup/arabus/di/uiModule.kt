package com.jibergroup.arabus.di

import com.jibergroup.arabus.ui.constructor.ConstructorViewModel
import com.jibergroup.arabus.data.repositories.WordRepository
import com.jibergroup.arabus.ui.details.WordDetailViewModel
import com.jibergroup.arabus.ui.favorites.FavoritesViewModel
import com.jibergroup.arabus.ui.search.SearchViewModel
import com.jibergroup.arabus.ui.settings.SettingsViewModel
import com.jibergroup.arabus.ui.sprint.SprintViewModel
import com.jibergroup.arabus.ui.test.TestViewModel
import com.jibergroup.arabus.ui.training_words.TrainingWordsViewModel
import com.jibergroup.arabus.ui.training_words_action.TrainingWordsActionViewModel
import kz.jibergroup.studyinn.presentation.course_test.SurveyViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {

    single {
        WordRepository(get(), get())
    }

    viewModel {
        SearchViewModel(get())
    }
    viewModel {
        WordDetailViewModel(get())
    }
    viewModel {
        FavoritesViewModel(get())
    }
    viewModel {
        TrainingWordsViewModel(get())
    }
    viewModel {
        TrainingWordsActionViewModel(get())
    }
    viewModel {
        SprintViewModel(get())
    }
    viewModel {
        TestViewModel()
    }
    viewModel {
        SurveyViewModel(get())
    }
    viewModel {
        ConstructorViewModel(get())
    }
    viewModel {
        SettingsViewModel(get())
    }

}