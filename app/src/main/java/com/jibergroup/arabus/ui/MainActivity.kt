package com.jibergroup.arabus.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.pref.PreferenceHelper
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var mNavController: NavController
    private var position = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        setLanguage()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        mNavController = host.navController

        navigationChangeListener(position)

        bottom_navigation_view.setNavigationChangeListener { _, position ->
            navigationChangeListener(position)
        }
    }

    private fun navigationChangeListener(position : Int){
        when (position) {
            0 -> openSearchFragment()
            2 -> openTrainingFragment()
            1 -> openFavoritesFragment()
            3 -> openSettingsFragment()
        }
    }

    private fun openSearchFragment() {
        if(position == 0){
            if(mNavController.currentDestination?.id == R.id.wordDetailFragment){
                mNavController.navigate(R.id.action_main_search_fragment)
            }
        }
        else {
            mNavController.navigate(R.id.action_main_search_fragment)
            position = 0
        }
    }

    private fun openFavoritesFragment(){
        if(position == 1){
            if(mNavController.currentDestination?.id == R.id.wordDetailFragment){
                mNavController.navigate(R.id.action_main_favorites_fragment)
            }
        }
        else{
            mNavController.navigate(R.id.action_main_favorites_fragment)
            position = 1
        }
    }

    private fun openTrainingFragment() {
        if(position != 2) {
            position = 2
            mNavController.navigate(R.id.action_main_game_fragment)
        }
    }

    private fun openSettingsFragment(){
        if(position!=3) {
            position = 3
            mNavController.navigate(R.id.action_main_settings_fragment)
        }
    }

    private val listener = NavController.OnDestinationChangedListener { controller, destination, arguments ->
        when (destination.id) {
            R.id.sprintFragment,R.id.constructorFragment,R.id.trainingWordsActionFragment,R.id.surveyFragment -> {
                bottom_navigation_view.visibility = View.GONE
            }
            else -> {
                bottom_navigation_view.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mNavController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        mNavController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }

    fun reopenActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun setLanguage(){
        val lang = PreferenceHelper.getLocaleLang()
        val locale = Locale(lang)
        val config = baseContext.resources.configuration
        Locale.setDefault(locale)
        config.locale = locale
        baseContext.resources.updateConfiguration(config,
            baseContext.resources.displayMetrics
        )
    }

    override fun onBackPressed() {
        if(mNavController.currentDestination == null
            || mNavController.currentDestination!!.id == R.id.searchFragment){
            super.onBackPressed()
        }
        if (mNavController.currentDestination?.id == R.id.favoritesFragment
            || mNavController.currentDestination?.id == R.id.trainingFragment
            || mNavController.currentDestination?.id == R.id.settingsFragment){
            mNavController.navigate(R.id.action_main_search_fragment)
            bottom_navigation_view.setCurrentActiveItem(0)
        }
        else{
            mNavController.navigateUp()
        }
    }
}