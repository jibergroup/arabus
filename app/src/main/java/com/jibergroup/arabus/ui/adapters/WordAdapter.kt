package com.jibergroup.arabus.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.utils.changeFavoriteImage
import kotlinx.android.synthetic.main.item_words_layout.view.*

class WordAdapter(
    private var wordsList: ArrayList<Word>,
    private val wordDetailClickListener: ((item: Word) -> Unit)? = null,
    private val wordChangeStateListener: ((word: Word,state: Boolean, position : Int) -> Unit)? = null,
    private val wordFavoritesClickListener: ((word: Word) -> Unit)? = null
) : RecyclerView.Adapter<WordAdapter.WordViewHolder>() {

    override fun getItemCount() = wordsList.size
    private var lastPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_words_layout, parent, false)
        return WordViewHolder(view)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val word = wordsList[position]
        holder.itemView.arabic_text.text = word.arabic
        holder.itemView.kazakh_text.text = word.text
        holder.itemView.arabic_second_text.text = word.arabicSecond
        holder.itemView.switchView.isChecked = word.isInGame
        holder.itemView.favoritesBtn.changeFavoriteImage(wordsList[holder.adapterPosition].isFavorite)

        holder.itemView.switchView.setOnCheckedChangeListener { buttonView , isChecked ->
            if(buttonView.isPressed){
                wordChangeStateListener?.invoke(wordsList[holder.adapterPosition], isChecked,holder.adapterPosition)
            }
        }

        holder.itemView.favoritesBtn.setOnClickListener {
            wordsList[holder.adapterPosition].isFavorite = !wordsList[holder.adapterPosition].isFavorite
            holder.itemView.favoritesBtn.changeFavoriteImage(wordsList[holder.adapterPosition].isFavorite)
            wordFavoritesClickListener?.invoke(wordsList[holder.adapterPosition])
        }

        holder.itemView.setOnClickListener {
            wordDetailClickListener?.invoke(wordsList[holder.adapterPosition])
        }

        setAnimation(holder.itemView, position, holder.itemView.context)
    }

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    fun setList(list: List<Word>) {
        lastPosition = 0
        wordsList.clear()
        wordsList.addAll(list)
        notifyDataSetChanged()

    }
    private fun setAnimation(viewToAnimate: View, position: Int, context: Context) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.push_left_in)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

}