package com.jibergroup.arabus.ui.constructor

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import java.util.*


class CapitalizedTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {
    override fun setText(t: CharSequence, type: BufferType) {
        val text = if (t.isNotEmpty()) {
            t[0].toString().uppercase(Locale.getDefault()) + t.subSequence(1, t.length)
        }else{
            t
        }
        super.setText(text, type)
    }
}