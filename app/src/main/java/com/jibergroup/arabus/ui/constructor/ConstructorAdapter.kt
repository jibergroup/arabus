package com.jibergroup.arabus.ui.constructor

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.global.base.BaseViewHolder
import com.jibergroup.arabus.ui.training_words.ItemClickListener
import kotlinx.android.synthetic.main.item_constructor.view.*

class ConstructorAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataSet = mutableListOf<VariantItem>()
    lateinit var listener: ItemClickListener

    fun submitData(menuList: MutableList<VariantItem>) {
        this.dataSet = menuList
        notifyDataSetChanged()
    }

    fun submitListener(listener: ItemClickListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_constructor
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_constructor -> ConstructorViewHolder(view)
            else -> ConstructorViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ConstructorViewHolder -> holder.bind(dataSet[position])
        }
    }

    inner class ConstructorViewHolder(itemView: View) : BaseViewHolder<VariantItem>(itemView) {

        override fun bind(item: VariantItem) {

            itemView.constructorItem.text = item.title

            if (item.isSelected) {
                itemView.isEnabled = false
                itemView.itemConstructor.background =
                    itemView.context.resources.getDrawable(R.drawable.shape_light_yellow)
            } else {
                itemView.isEnabled = true
                itemView.itemConstructor.background =
                    itemView.context.resources.getDrawable(R.drawable.shape_orange)
                itemView.setOnClickListener {
                    listener.onClickItem(adapterPosition)
                }
            }


        }

    }

}
