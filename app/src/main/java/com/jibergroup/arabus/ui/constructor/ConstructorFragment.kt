package com.jibergroup.arabus.ui.constructor

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.ui.global.base.alertWithActions
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import com.jibergroup.arabus.ui.sprint.TrainingProgress
import com.jibergroup.arabus.ui.training.TRAIN_ITEM_KEY
import com.jibergroup.arabus.ui.training.TrainingItem
import com.jibergroup.arabus.ui.training_words.ItemClickListener
import kotlinx.android.synthetic.main.constructor_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class ConstructorFragment : Fragment(R.layout.constructor_fragment), ItemClickListener {

    val viewModel: ConstructorViewModel by viewModel()

    lateinit var constructorAdapter: ConstructorAdapter
    lateinit var scaleAnimation: Animation


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments?.getParcelable<TrainingItem>(TRAIN_ITEM_KEY)?.let {
            constructorToolbarTitle.text = it.title
            viewModel.trainItem.value = it
        }

        initView()


        viewModel.constructorProgress.observe(activity as AppCompatActivity, actionObserver)
        viewModel.words.observe(activity as AppCompatActivity, wordsObserver)
        viewModel.currentWordOrder.observe(activity as AppCompatActivity, currentWordOrderObserver)
        viewModel.currentWord.observe(activity as AppCompatActivity, currentWordObserver)
        viewModel.userOrder.observe(activity as AppCompatActivity, wordCompletenessObserver)
        viewModel.answerRandom.observe(activity as AppCompatActivity, randomDataObserver)

        constructorToolbarBack.setOnClickListener {
            backAction()
        }

        constructorBack.setOnClickListener {
            backSelect()
        }

        scaleAnimation = AnimationUtils.loadAnimation(activity, R.anim.scale)
        scaleAnimation.setAnimationListener(animationObjectListener)


        customDialogClose.setOnClickListener {
            viewModel.switchNextAnswer()
        }
    }

    override fun onResume() {
        super.onResume()
        if (view == null) {
            return
        }
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showExitDialog()
                true
            } else false
        }
    }


    private val randomDataObserver = Observer<List<Word>> {
        viewModel.getConstructorData()
    }

    private val wordsObserver = Observer<MutableList<ConstructorWordItem>> {
        if (it.isNotEmpty()) {
            viewModel.startGame()
            constructorContainer.visibility = View.VISIBLE
        } else {
            alertGameWordsEmpty()
        }
    }


    private fun alertGameWordsEmpty() {
        activity?.createCustomDialog(titleText = getString(R.string.trainings),
            messageText = getString(R.string.sprint_words_empty),
            dismissCallback = {
                activity?.onBackPressed()
            })
    }

    private val wordCompletenessObserver = Observer<Stack<Int>> { order ->
        if (viewModel.currentWord.value?.variants?.size == order.size) {

            var word = ""
            val listOrder = mutableListOf<VariantItemCopy>()
            viewModel.currentWord.value?.variants?.forEach { variant ->
                listOrder.add(
                    VariantItemCopy(
                        id = variant.id,
                        isSelected = variant.isSelected,
                        title = variant.title,
                        order = variant.order
                    )
                )
            }
            listOrder.sort()

            listOrder.forEach {
                word = word.plus(it.title)
            }

            if (word.equals(constructorMain.text.toString().toLowerCase())) {
                doCorrectAction()
            } else {
                doIncorrectAction()
            }

        }
    }

    private fun vibrate() {
        val v = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v.vibrate(500)
        }

    }

    private fun doIncorrectAction() {
        vibrate()

        val shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.shake)
        shakeAnimation.setAnimationListener(shakeAnimationObjectListener)
        constructorMain.startAnimation(shakeAnimation)
    }

    private val shakeAnimationObjectListener = object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation) {}
        override fun onAnimationEnd(animation: Animation) {
            constructorMain.text = ""
            viewModel.resetWord()
        }

        override fun onAnimationRepeat(animation: Animation) {}
    }

    private fun doCorrectAction() {
        viewModel.incrementCorrectAnswer()
        constructorBack.setImageDrawable(resources.getDrawable(R.drawable.ic_check_small))
        constructorBack.startAnimation(scaleAnimation)
    }

    private val animationObjectListener = object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation) {}
        override fun onAnimationEnd(animation: Animation) {
            constructorBack.setImageDrawable(resources.getDrawable(R.drawable.ic_remove))
            viewModel.switchNextAnswer()
            constructorMain.text = ""
        }

        override fun onAnimationRepeat(animation: Animation) {}
    }


    private val currentWordOrderObserver = Observer<Int> {
        viewModel.currentWord.value = viewModel.words.value?.get(it)
    }

    private val currentWordObserver = Observer<ConstructorWordItem> {
        constructorAdapter.submitData(it?.variants ?: mutableListOf())
        constructorWord.text = it.title
    }


    override fun onClickItem(position: Int) {
        constructorMain.text = constructorMain.text.toString()
            .plus(viewModel.currentWord.value?.variants?.get(position)?.title)
        viewModel.selectVariant(position)
    }


    private fun initView() {

        constructorAdapter = ConstructorAdapter()
        constructorAdapter.submitListener(this)

        val layoutManager = FlexboxLayoutManager(activity)
        layoutManager.setFlexWrap(FlexWrap.WRAP)
        layoutManager.setFlexDirection(FlexDirection.ROW)
        layoutManager.setAlignItems(AlignItems.STRETCH)

        constructorAnswerRecycler.layoutManager = layoutManager
        constructorAnswerRecycler.adapter = constructorAdapter
    }


    private fun backSelect() {
        if (constructorMain.text.isNotEmpty()) {
            constructorMain.text =
                constructorMain.text.subSequence(0, constructorMain.text.length - 1)
            viewModel.backSelectVariant()
        }
    }


    fun onCompleteConstructor() {
        activity?.onBackPressed()
    }


    private val actionObserver = Observer<TrainingProgress> {
        when (it) {
            TrainingProgress.Started -> {
//                viewModel.initData()
            }
            TrainingProgress.Finished -> {
                showResultDialog()
            }

            else -> {
            }
        }
    }

    private fun showResultDialog() {
        activity?.createCustomDialog(titleText = getString(R.string.trainings),
            messageText = getString(
                R.string.your_test_result,
                viewModel.words.value?.size.toString(),
                viewModel.correctAnswer.value.toString()
            ),
            dismissCallback = {
                onCompleteConstructor()
            })
    }

    private fun backAction() {
        when (viewModel.constructorProgress.value) {
            TrainingProgress.Started -> {
              showExitDialog()
            }
            else -> {
                onCompleteConstructor()
            }
        }
    }

    private fun showExitDialog(){
        context?.alertWithActions(
            title = getString(R.string.attention),
            message = getString(R.string.sure_to_cancel_training),
            positiveButtonCallback = {
                onCompleteConstructor()
            },
            cancelable = false,
            negativeButtonCallback = {
            }, negativeText = getString(R.string.cancel)
        )
    }

}