package com.jibergroup.arabus.ui.constructor

import android.view.animation.Animation
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.repositories.WordRepository
import com.jibergroup.arabus.ui.sprint.TrainingProgress
import com.jibergroup.arabus.ui.training.TrainingItem
import com.jibergroup.arabus.ui.training_words.GameData
import kotlinx.android.synthetic.main.constructor_fragment.*
import kotlinx.coroutines.launch
import java.text.Normalizer
import java.util.*
import kotlin.random.Random

class ConstructorViewModel(private val repository: WordRepository) : ViewModel() {

    val trainItem = MutableLiveData<TrainingItem>()
    val constructorProgress = MutableLiveData<TrainingProgress>()
    val words = MutableLiveData<MutableList<ConstructorWordItem>>()
    val currentWordOrder = MutableLiveData<Int>()
    val currentWord = MutableLiveData<ConstructorWordItem>()
    val correctAnswer = MutableLiveData<Int>()

    val userOrder = MutableLiveData<Stack<Int>>()

    val answerRandom = MutableLiveData<List<Word>>()
    val data = MutableLiveData<List<GameData>>()


    init {
        getRandomData()
    }

    fun startGame(){
        userOrder.value = Stack()
        currentWordOrder.value = 0
        correctAnswer.value = 0
        constructorProgress.value = TrainingProgress.Started
    }

    fun getRandomData() {
        viewModelScope.launch {
            answerRandom.value = repository.loadRandomAnswer()
        }
    }

    fun getConstructorData() {
        viewModelScope.launch {
            data.value = repository.onLoadTrainingWords().filter { it.constructor == true }
            initData()
        }
    }

    private fun initData() {


        val answers = mutableListOf<ConstructorWordItem>()


        data.value?.forEach {

            val variants = mutableListOf<VariantItem>()

            it.arabic?.forEachIndexed { index, letter ->
                if(letter.isLetter()){
                    variants.add(VariantItem(id = Random.nextLong(), title = letter.toString(), order = index.plus(1), isSelected = false))
                }
            }

            variants.shuffle()

            answers.add(ConstructorWordItem(title = it.text, variants = variants))

        }

        words.value = answers
    }


    fun selectVariant(position: Int) {

        val currentOrder = userOrder.value
        currentOrder?.push(position)

        val current = currentWord.value
        current?.variants?.get(position)?.isSelected = true

        currentWord.value = current
        userOrder.value = currentOrder

    }

    fun backSelectVariant() {

        userOrder.value?.pop()?.let {
            val current = currentWord.value
            current?.variants?.get(it)?.isSelected = false

            currentWord.value = current
        }


    }

    fun resetWord(){
        val current = currentWord.value
        current?.variants?.forEach {
            it.isSelected = false
        }
        currentWord.value = current
        resetOrder()
    }

    fun incrementCorrectAnswer(){
        correctAnswer.value = correctAnswer.value?.plus(1)
    }


    fun switchNextAnswer(){

        if(currentWordOrder.value?.compareTo(words.value?.size ?: -1) == -1){
            if(currentWordOrder.value?.compareTo(words.value?.size?.minus(1) ?: -1) == 0){
                constructorProgress.value = TrainingProgress.Finished
            }else{
                currentWordOrder.value = currentWordOrder.value?.plus(1)
                resetOrder()
            }
        }
    }

    fun resetOrder(){
        userOrder.value = Stack()
    }


}

class ConstructorWordItem(
    val title: String? = null,
    val variants: MutableList<VariantItem>? = null
)



data class VariantItem(
    val id: Long,
    val title: String,
    val order: Int,
    var isSelected: Boolean
)

data class VariantItemCopy(
    val id: Long? = null,
    val title: String? = null,
    val order: Int? = null,
    var isSelected: Boolean? = null
) : Comparable<VariantItemCopy>{
    override fun compareTo(other: VariantItemCopy): Int {
        return this.order?.compareTo(other.order ?: -1) ?: -1
    }
}

//shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.shake)
//shakeAnimation.setAnimationListener(shakeAnimationObjectListener)
//private val shakeAnimationObjectListener = object : Animation.AnimationListener {
//    override fun onAnimationStart(animation: Animation) {}
//    override fun onAnimationEnd(animation: Animation) {
//        constructorMain.text = ""
//        viewModel.resetWord()
//    }
//
//    override fun onAnimationRepeat(animation: Animation) {}
//}