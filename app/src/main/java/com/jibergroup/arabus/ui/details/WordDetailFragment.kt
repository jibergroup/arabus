package com.jibergroup.arabus.ui.details

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jibergroup.arabus.BuildConfig
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.pref.APP_BASE
import com.jibergroup.arabus.data.pref.PreferenceHelper
import com.jibergroup.arabus.ui.adapters.SimpleWordAdapter
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import com.jibergroup.arabus.utils.changeFavoriteImage
import com.jibergroup.arabus.utils.copyToClipboard
import com.jibergroup.arabus.utils.share
import kotlinx.android.synthetic.main.fragment_word_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.concurrent.schedule

class WordDetailFragment : Fragment(R.layout.fragment_word_detail) {

    private val mWordDetailViewModel: WordDetailViewModel by viewModel()
    private val mRootWordsAdapter: SimpleWordAdapter =
        SimpleWordAdapter { word -> onItemClickListener(word) }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.getSerializable("word")?.let {
            val word = it as Word
            initWordData(word)
        }

        rootWordRecyclerView.isNestedScrollingEnabled = false
        rootWordRecyclerView.setHasFixedSize(false)
        rootWordRecyclerView.adapter = mRootWordsAdapter

        listeners()
        initObservables()

        mWordDetailViewModel.onUpdateWordHistory(Date())

    }

    private fun initWordData(word: Word) {
        toolbar_title.text = word.arabic
        arabic_text.text = word.arabic
        description_text.text = word.description
        arabic_second_text.text = word.arabicSecond

        mWordDetailViewModel.initWord(word)
        switchView.isChecked = word.isInGame
        favoritesBtn.changeFavoriteImage(word.isFavorite)
    }

    private fun listeners() {
        backBtn.setOnClickListener {
            Timer().schedule(100) {
                findNavController().navigateUp()
            }
        }

        copyBtn.setOnClickListener {
            activity?.copyToClipboard("\u200E" + mWordDetailViewModel.getWord()?.arabic
                ?.plus("\u200E " + mWordDetailViewModel.getWord()?.arabicSecond)
                .plus("\n").plus(mWordDetailViewModel.getWord()?.description)
                .plus("\n\n" + getString(R.string.app_full_name)+
                        "\nhttps://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"))
            Toast.makeText(
                activity,
                getString(R.string.text_successfully_copied),
                Toast.LENGTH_SHORT
            ).show()
        }

        shareBtn.setOnClickListener {
            shareBtn.share(
                requireActivity().applicationContext,
                "\u200E" + mWordDetailViewModel.getWord()?.arabic
                    ?.plus("\u200E " + mWordDetailViewModel.getWord()?.arabicSecond)
                    .plus("\n").plus(mWordDetailViewModel.getWord()?.description)
                    .plus("\n\n" + getString(R.string.app_full_name)+
                            "\nhttps://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"))
        }

        favoritesBtn.setOnClickListener {
            mWordDetailViewModel.getWord()?.isFavorite?.let {
                favoritesBtn.changeFavoriteImage(!it)
                mWordDetailViewModel.onChangeWordFavoriteState()
            } 
        }

        switchView.setOnCheckedChangeListener { buttonView, isChecked ->
            if (buttonView.isPressed) {
                if(isAvailable(isChecked)){
                    mWordDetailViewModel.getWord()?.let {
                        mWordDetailViewModel.updateStateToSprint(it, isChecked)
                    }
                }else{
                    showRestrictDialog()
                }
            }
        }

    }

    private fun isAvailable(state: Boolean) : Boolean{
        return if(mWordDetailViewModel.getAppAccessibility() == APP_BASE) {
            PreferenceHelper.checkAccess(state)
        }else{
            true
        }
    }

    private fun showRestrictDialog(){
        activity?.createCustomDialog(getString(R.string.attention),getString(R.string.restrict_full_version_info),{},{
            switchView.isChecked = mWordDetailViewModel.getWord()?.isInGame ?: false
        })
    }

    override fun onResume() {
        super.onResume()
        scrollView.smoothScrollTo(0, 0)
    }

    private fun initObservables() {
        mWordDetailViewModel.wordList.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { list ->
                if (!list.isNullOrEmpty()) {
                    mRootWordsAdapter.addList(list)
                    if (rootWordLayout.visibility == View.GONE) {
                        rootWordLayout.visibility = View.VISIBLE
                    }
                } else {
                    rootWordLayout.visibility = View.GONE
                }
            })
    }

    private fun onItemClickListener(word: Word){
        val args = Bundle()
        args.putSerializable("word", word)
        findNavController().navigate(R.id.action_reload, args)
    }

}