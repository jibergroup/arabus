package com.jibergroup.arabus.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.repositories.WordRepository
import kotlinx.coroutines.launch
import java.util.*

class WordDetailViewModel(
    private val wordRepository: WordRepository
): ViewModel(){

    private var word: Word? = null
    fun initWord(word: Word){
        this.word = word
        onLoadRootWords(word.root, word.id)
    }

    fun getWord() : Word? = word

    private var _wordList = MutableLiveData<List<Word>>()
    val wordList = _wordList
    private fun onLoadRootWords(root: String, wordId: Int){
        viewModelScope.launch {
            _wordList.value = wordRepository.loadRootWords(root, wordId)
        }
    }

    fun getAppAccessibility() : Int{
        return wordRepository.getAppAccessibility()
    }


    fun onChangeWordFavoriteState(){
        viewModelScope.launch {
            word?.let{
                it.isFavorite = !it.isFavorite
                wordRepository.onUpdateWordFavorite(it)
            }
        }
    }

    fun onUpdateWordHistory(viewedAt: Date){
        viewModelScope.launch {
            word?.let {
                wordRepository.onUpdateWordHistory(it.id, viewedAt)
            }
        }
    }

    fun updateStateToSprint(word: Word, state: Boolean) {
        viewModelScope.launch {
            wordRepository.updateStateToSprint(word.id, state)
        }
    }

}