package com.jibergroup.arabus.ui.favorites

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.pref.APP_BASE
import com.jibergroup.arabus.data.pref.LIMIT
import com.jibergroup.arabus.data.pref.PreferenceHelper
import com.jibergroup.arabus.ui.adapters.WordAdapter
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.android.synthetic.main.fragment_search.recyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.collections.ArrayList

class FavoritesFragment : Fragment(R.layout.fragment_favorites) {

    private val mViewModel: FavoritesViewModel by viewModel()

    private val adapter = WordAdapter(wordsList = ArrayList(),
        wordChangeStateListener = { word,state,position -> onWordStateChangeListener(word,state,position) },
        wordFavoritesClickListener = {word -> onFavoriteClickListener(word)},
        wordDetailClickListener = { word -> onWordClickListener(word) }
    )

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.adapter = adapter
        mViewModel.onLoadFavoriteWords()

        mViewModel.wordModelList.observe(viewLifecycleOwner, Observer { list ->
            checkForView(list)
        })

    }

    private fun checkForView(data : List<Word>){
        if(data.isNotEmpty()){
            recyclerView.visibility = View.VISIBLE
            empty_layout_fav.visibility = View.GONE
            adapter.setList(data)
        }else{
            recyclerView.visibility = View.GONE
            empty_layout_fav.visibility = View.VISIBLE
        }
    }

    private fun onWordStateChangeListener(word: Word, state: Boolean, position : Int){
        if(isAvailable(state)){
            mViewModel.updateStateToSprint(word,state)
        }else{
            showRestrictDialog(position)
        }
    }

    private fun isAvailable(state: Boolean) : Boolean{
        return if(mViewModel.getAppAccessibility() == APP_BASE) {
            PreferenceHelper.checkAccess(state)
        }else{
            true
        }
    }

    private fun showRestrictDialog(position: Int){
        activity?.createCustomDialog(getString(R.string.attention),getString(R.string.restrict_full_version_info),{},{
            adapter.notifyItemChanged(position)
        })
    }
    private fun onFavoriteClickListener(word: Word){
        mViewModel.onUpdateWordFavorite(word)
    }

    private fun onWordClickListener(word: Word) {
        val args = Bundle()
        args.putSerializable("word", word)
        findNavController().navigate(R.id.action_favoritesFragment_to_wordDetailFragment, args)
    }

}