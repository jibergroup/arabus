package com.jibergroup.arabus.ui.global.base


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.jibergroup.arabus.R

const val PREF_NAME_FIELD = "stateFiled"

fun Context.toast(message: String, longToast: Boolean = true) {
    Toast.makeText(this, message, if (longToast) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
}

enum class MessageType {
    ALERT,
    TOAST
}

fun Context.alert(
    title: String? = null,
    message: String? = getString(R.string.error_message),
    positiveButton: (() -> Unit?)? = null,
    cancelable: Boolean = true,
    type: MessageType = MessageType.ALERT
) {
    when (type) {
        MessageType.TOAST -> {
            this.toast(message ?: "Что-то пошло не так")
        }
        MessageType.ALERT -> {
            val dialog = AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes) { dialog, _ ->
                    dialog.dismiss()
                    positiveButton?.invoke()
                }
                .setCancelable(cancelable)
                .create()
            if(title != null)
                dialog.setTitle(title)

            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            }
            dialog.show()
        }
    }
}

fun Context.alertWithActions(
    title: String,
    message: String,
    cancelable: Boolean = true,
    positiveButtonCallback: () -> Unit,
    negativeButtonCallback: () -> Unit,
    positiveText: String? = getString(R.string.ok),
    negativeText: String? = resources.getString(R.string.cancel)
) {

    val dialog = AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(positiveText) { dialog, _ ->
            positiveButtonCallback()
            dialog.dismiss()
        }
        .setNegativeButton(negativeText) { dialog, _ ->
            negativeButtonCallback()
            dialog.dismiss()
        }
        .setCancelable(cancelable)
        .create()

    dialog.setOnShowListener {
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
    }
    dialog.show()
}

fun Context.createCustomDialog(
    titleText: String,
    messageText: String,
    positiveButton: (() -> Unit?)? = null,
    dismissCallback: () -> Unit,
    buttonText: String? = getString(R.string.close)
){
    val dialog = Dialog(this)
    dialog.setContentView(R.layout.custom_dialog)

    val width = (resources.displayMetrics.widthPixels * 0.9).toInt()
    val height = ConstraintLayout.LayoutParams.WRAP_CONTENT

    dialog.getWindow()?.setLayout(width, height)
    dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    val title = dialog.findViewById(R.id.customDialogTitle) as TextView
    title.text = titleText

    val result = dialog.findViewById(R.id.customDialogMessage) as TextView
    result.text = messageText

    val close = dialog.findViewById(R.id.customDialogClose) as Button
    close.text = buttonText

    dialog.show()

    val declineButton = dialog.findViewById(R.id.customDialogClose) as Button
    declineButton.setOnClickListener {
        dialog.dismiss()
        positiveButton?.invoke()
    }

    dialog.setOnDismissListener {
        dismissCallback()
    }
}
