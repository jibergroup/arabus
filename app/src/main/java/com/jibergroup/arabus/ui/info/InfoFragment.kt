package com.jibergroup.arabus.ui.info

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jibergroup.arabus.R
import kotlinx.android.synthetic.main.fragment_info.*


class InfoFragment : Fragment(R.layout.fragment_info) {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        backBtn.setOnClickListener {
            findNavController().navigateUp()
        }

        info_university.setOnClickListener {
            openLinkDialog()
        }

    }

    private fun openLinkDialog(){
        val dialog = Dialog(activity as AppCompatActivity)
        dialog.setContentView(R.layout.custom_support)

        dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val width = (resources.displayMetrics.widthPixels * 0.8).toInt()
        val height = ConstraintLayout.LayoutParams.WRAP_CONTENT

        dialog.getWindow()?.setLayout(width, height);


        val telegram = dialog.findViewById(R.id.telegram) as TextView
        val instagram = dialog.findViewById(R.id.instagram) as TextView
        val website = dialog.findViewById(R.id.website) as TextView


        telegram.setOnClickListener {
            openTelegram()
        }

        instagram.setOnClickListener {
            openInstagram()
        }

        website.setOnClickListener {
            openWeb(getString(R.string.site_url))
        }


        dialog.show()

        val declineButton = dialog.findViewById(R.id.customDialogClose) as Button
        declineButton.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun openInstagram(){
        var url = getString(R.string.instagram_link)
        val pm = activity?.packageManager
        val intent = Intent(Intent.ACTION_VIEW)
        try {
            if (pm?.getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length - 1)
                }
                val username: String = url.substring(url.lastIndexOf("/") + 1)
                intent.data = Uri.parse("http://instagram.com/_u/$username")
                intent.setPackage("com.instagram.android")
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }
        intent.data = Uri.parse(url)
        startActivity(intent)

    }

    private fun openTelegram(){

        var uri = Uri.parse("tg://resolve?domain=nmu_kz")
        val telegramPackage = "org.telegram.messenger"

        if(!isAppAvailable(activity as AppCompatActivity, telegramPackage)){
            uri = Uri.parse(getString(R.string.telegram_link))
        }

        val intent =
            Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    private fun isAppAvailable(context: Context, appName: String?): Boolean {
        val pm: PackageManager = context.packageManager
        return try {
            appName?.let { pm.getPackageInfo(it, PackageManager.GET_ACTIVITIES) }
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun openWeb(url : String){
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

}