package com.jibergroup.arabus.ui.search

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.pref.APP_BASE
import com.jibergroup.arabus.data.pref.PreferenceHelper
import com.jibergroup.arabus.ui.adapters.WordAdapter
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import com.jibergroup.arabus.utils.Constants.ARABIC_LETTERS
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

private const val REQUEST_WRITE_PERMISSION = 123

class SearchFragment : Fragment(R.layout.fragment_search),
    ActivityCompat.OnRequestPermissionsResultCallback {

    private val searchViewModel: SearchViewModel by viewModel()
    private val adapter =
        WordAdapter(wordsList = ArrayList(),
            wordDetailClickListener = { word -> onWordClickListener(word) },
            wordChangeStateListener = {word,state,position -> onWordStateChangeListener(word,state,position = position )},
            wordFavoritesClickListener = {word-> onFavoriteClickListener(word)}
        )

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requestPermission()

        recyclerView.isNestedScrollingEnabled = false
        recyclerView.adapter = adapter

        listeners()
        initObservables()

        searchViewModel.onSearch(null)
        search_view.setSelection(0)

    }

    private fun listeners() {
        clear_btn.setOnClickListener {
            search_view.setText("")
        }
    }

    private fun initObservables() {

        search_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchViewModel.cancelJob()
                val searchText = search_view.text.toString().trim().toLowerCase(Locale.getDefault())
                val isArabic = isArabic(searchText)
                searchViewModel.onSearch(searchText, isArabic)
            }
        })

        searchViewModel.searchLiveData.observe(viewLifecycleOwner, Observer { searchState ->
            when (searchState) {
                SearchState.SEARCHABLE -> {
                    showClearButton()
                }
                SearchState.EMPTY -> {
                    hideClearButton()
                }
                SearchState.NONE -> {
                    hideClearButton()
                }
            }
        })

        searchViewModel.wordModelList.observe(viewLifecycleOwner, Observer { list ->
            list?.let {
                adapter.setList(list)
                if(list.isNotEmpty()) {
                    recyclerView.smoothScrollToPosition(0)
                }
                checkForView(list)
            }
        })
    }

    private fun checkForView(data : List<Word>){
        if(data.isNotEmpty()){
            recyclerView.visibility = View.VISIBLE
            empty_layout.visibility = View.GONE
        }else{
            recyclerView.visibility = View.GONE
            empty_layout.visibility = View.VISIBLE
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) { }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                REQUEST_WRITE_PERMISSION
            )
        } else {
            searchViewModel.onSearch(null)
        }
    }

    private fun hideClearButton() {
        clear_btn.visibility = View.INVISIBLE
    }

    private fun showClearButton() {
        if (clear_btn.visibility == View.INVISIBLE) {
            clear_btn.visibility = View.VISIBLE
        }
    }

    private fun onWordClickListener(word: Word) {
        val args = Bundle()
        args.putSerializable("word", word)
        findNavController().navigate(R.id.action_searchFragment_to_wordDetailFragment, args)
    }

    private fun onWordStateChangeListener(word: Word, state: Boolean, position : Int){
        if(isAvailable(state)){
            searchViewModel.updateStateToSprint(word,state)
        }else{
            showRestrictDialog(position)
        }
    }

    private fun isAvailable(state: Boolean) : Boolean{
        return if(searchViewModel.getAppAccessibility() == APP_BASE) {
            PreferenceHelper.checkAccess(state)
        }else{
            true
        }
    }

    private fun showRestrictDialog(position: Int){
        activity?.createCustomDialog(getString(R.string.attention),getString(R.string.restrict_full_version_info),{},{
            adapter.notifyItemChanged(position)
        })
    }

    private fun onFavoriteClickListener(word: Word){
        searchViewModel.onUpdateWordFavorite(word)
    }

    private fun isArabic(word: String): Boolean{
        for(character in word.toCharArray()){
            if(ARABIC_LETTERS.indexOf(character) != -1){
                return true
            }
        }
        return false
    }

}