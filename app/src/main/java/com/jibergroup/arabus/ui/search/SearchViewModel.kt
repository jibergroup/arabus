package com.jibergroup.arabus.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.repositories.WordRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchViewModel(
    private val repository: WordRepository
) : ViewModel() {

    private var searchMutableLiveData = MutableLiveData<SearchState>()
    val searchLiveData = searchMutableLiveData
    var searchJob : Job = Job()

    init {
        searchLiveData.value = SearchState.NONE
    }

    fun getAppAccessibility() : Int{
        return repository.getAppAccessibility()
    }

    fun onSearch(similarWord: String?, isArabic: Boolean = false) {
        if (similarWord.isNullOrBlank()) {
            searchMutableLiveData.value = SearchState.EMPTY
            loadHistories()
        } else {
            searchMutableLiveData.value = SearchState.SEARCHABLE
            loadWords(similarWord, isArabic)
        }
    }

    private var _wordModelList = MutableLiveData<List<Word>>()
    val wordModelList = _wordModelList
    private fun loadWords(similarWord: String, isArabic: Boolean) {
        searchJob = viewModelScope.launch {
            _wordModelList.value = repository.loadWords(similarWord, isArabic)
        }
    }

    private fun loadHistories() {
        viewModelScope.launch {
            _wordModelList.value = repository.loadWordHistories()
        }
    }

    fun onUpdateWordFavorite(word: Word) {
        viewModelScope.launch {
            repository.onUpdateWordFavorite(word)
        }
    }

    fun updateStateToSprint(word: Word, state: Boolean) {
        viewModelScope.launch {
            repository.updateStateToSprint(word.id, state)
        }
    }

    fun cancelJob(){
        if(searchJob != null && searchJob.isActive) {
            searchJob.cancel()
        }
    }
}