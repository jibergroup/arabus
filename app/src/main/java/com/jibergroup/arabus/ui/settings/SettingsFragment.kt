package com.jibergroup.arabus.ui.settings

import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.billingclient.api.*
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.pref.APP_BASE
import com.jibergroup.arabus.data.pref.APP_FULL
import com.jibergroup.arabus.data.pref.LIMIT
import com.jibergroup.arabus.data.pref.PreferenceHelper
import com.jibergroup.arabus.ui.MainActivity
import com.jibergroup.arabus.ui.global.base.alert
import com.jibergroup.arabus.ui.global.base.alertWithActions
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import kotlinx.android.synthetic.main.dialog_language.*
import kotlinx.android.synthetic.main.fragment_settings.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class SettingsFragment: Fragment(R.layout.fragment_settings){

    private val viewModel: SettingsViewModel by viewModel()
    lateinit var mBillingClient: BillingClient
    private var mSkuDetailsMap = hashMapOf<String, SkuDetails>()
    private val mSkuId = "arabist_full_version"
    lateinit var dialog : Dialog

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        checkForVersion()

        infoLayout.setOnClickListener {
            findNavController().navigate(R.id.actionOpenInfoFragment)
        }

        change_language.setOnClickListener {
            showDialog()
        }

        delete_history.setOnClickListener {
            requireActivity().alertWithActions(
                title = getString(R.string.attention),
                message = getString(R.string.sure_to_delete_history),
                positiveButtonCallback = {
                    viewModel.onDeleteHistory()
                },
                cancelable = false,
                negativeButtonCallback = {})
        }

        viewModel.deleteHistory.observe(viewLifecycleOwner, androidx.lifecycle.Observer { success->
            if(success){
                Toast.makeText(requireActivity(), getString(R.string.successfully_deleted), Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(requireActivity(), getString(R.string.unsuccess_deleted), Toast.LENGTH_SHORT).show()
            }
        })

        subscribeBtn.setOnClickListener {
            launchBilling(mSkuId)
        }

        mBillingClient =
            BillingClient.newBuilder(activity as AppCompatActivity).setListener { responseCode, purchases ->
                if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
                    payComplete()
                }
            }.build()

        mBillingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    querySkuDetails()
                    val purchasesList = queryPurchases()
                    for (i in purchasesList!!.indices) {
                        val purchaseId = purchasesList[i]?.sku
                        if (TextUtils.equals(mSkuId, purchaseId)) {
                            payComplete()
                        }
                    }
                }
            }

            override fun onBillingServiceDisconnected() {
                context?.alert(getString(R.string.attention))
            }
        })

    }

    private fun payComplete(){
        viewModel.setAppAccessibility(APP_FULL)
        checkForVersion()
    }

    fun checkForVersion(){
        if(viewModel.getAppAccessibility() == APP_BASE){
            fullVersionCard.visibility = View.VISIBLE
            fullVersionTitle.visibility = View.VISIBLE
        }else{
            fullVersionCard.visibility = View.GONE
            fullVersionTitle.visibility = View.GONE
        }
    }

    fun launchBilling(skuId: String?) {
        val billingFlowParams = BillingFlowParams.newBuilder()
            .setSkuDetails(mSkuDetailsMap[skuId])
            .build()
        mBillingClient.launchBillingFlow(activity as AppCompatActivity, billingFlowParams)
    }

    private fun queryPurchases(): List<Purchase?>? {
        val purchasesResult =
            mBillingClient.queryPurchases(BillingClient.SkuType.INAPP)
        return purchasesResult.purchasesList
    }

    private fun querySkuDetails() {
        val skuDetailsParamsBuilder = SkuDetailsParams.newBuilder()
        val skuList: MutableList<String> = ArrayList()
        skuList.add(mSkuId)
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.INAPP)
        mBillingClient.querySkuDetailsAsync(
            skuDetailsParamsBuilder.build()
        ) { responseCode, skuDetailsList ->
            if (responseCode == 0) {
                for (skuDetails in skuDetailsList) {
                    mSkuDetailsMap.put(skuDetails.sku, skuDetails)
                }
            }
        }
    }

    private fun showDialog() {
        dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_language)

        val isKazakhLang = PreferenceHelper.localeLangIsKazakh()
        languageGroup(isKazakhLang)
        dialog.kazLayout.setOnClickListener {
            if(isKazakhLang){
                dialog.cancel()
            }
            else{
                languageGroup(true)
                changeLanguage(true)
            }
        }

        dialog.rusLayout.setOnClickListener {
            if(isAvailable()){
                if(isKazakhLang){
                    languageGroup(false)
                    changeLanguage(false)
                }
                else{
                    dialog.cancel()
                }
            }else{
                dialog.cancel()
                showRestrictDialog()
            }
        }
        dialog.show()
    }

    private fun isAvailable() : Boolean{
        return viewModel.getAppAccessibility() != APP_BASE
    }

    private fun showRestrictDialog(){
        activity?.createCustomDialog(getString(R.string.attention),getString(R.string.restrict_full_version_info),{},{})
    }

    private fun languageGroup(check: Boolean){
        dialog.checkKk.isChecked = check
        dialog.checkRu.isChecked = !check
    }

    private fun changeLanguage(isKazakhLang: Boolean){
        val lang = if (isKazakhLang) "kk" else "ru"
        PreferenceHelper.setLocaleLang(lang)
        setLanguage(lang)

        (requireActivity() as MainActivity).reopenActivity()
    }

    private fun setLanguage(lang: String){
        val locale = Locale(lang)
        val config = requireActivity().resources.configuration
        Locale.setDefault(locale)
        config.locale = locale
        requireActivity().resources.updateConfiguration(config,
            requireActivity().resources.displayMetrics
        )
    }

}