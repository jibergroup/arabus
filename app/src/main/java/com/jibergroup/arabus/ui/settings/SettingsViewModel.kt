package com.jibergroup.arabus.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.repositories.WordRepository
import kotlinx.coroutines.launch

class SettingsViewModel(
    private val repository: WordRepository
) : ViewModel(){

    private val _deleteHistory = MutableLiveData<Boolean>()
    val deleteHistory: LiveData<Boolean> = _deleteHistory
    fun onDeleteHistory(){
        viewModelScope.launch {
            _deleteHistory.value = repository.deleteHistories()
        }
    }

    fun getAppAccessibility() : Int{
        return repository.getAppAccessibility()
    }

    fun setAppAccessibility(app : Int) {
        return repository.setAppAccessibility(app)
    }

}