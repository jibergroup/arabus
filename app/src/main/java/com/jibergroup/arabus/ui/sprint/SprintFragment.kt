package com.jibergroup.arabus.ui.sprint

import android.os.Bundle
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.ankushgrover.hourglass.Hourglass
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.ui.global.base.alertWithActions
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import com.jibergroup.arabus.ui.training_words.GameData
import kotlinx.android.synthetic.main.sprint_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SprintFragment : Fragment(R.layout.sprint_fragment) {

    val viewModel: SprintViewModel by viewModel()
    lateinit var countDownTimerStart: CountDownTimer
    lateinit var countDownTimerSprint: Hourglass

    lateinit var alphaAnimation: Animation

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        sprintToolbarTitle.text = getString(R.string.sprint)

        countDownTimerStart = timerObjectStart
        countDownTimerSprint = timerObjectSprint

        viewModel.sprintProgress.observe(activity as AppCompatActivity, sprintProgressObserver)
        viewModel.currentAnswer.observe(activity as AppCompatActivity, currentAnswerObserver)
        viewModel.correctAnswer.observe(activity as AppCompatActivity, correctAnswerObserver)
        viewModel.data.observe(activity as AppCompatActivity, dataObserver)
        viewModel.answerRandom.observe(activity as AppCompatActivity, randomDataObserver)

        alphaAnimation = AnimationUtils.loadAnimation(activity, R.anim.alpha)
        alphaAnimation.setAnimationListener(animationObjectListener)

        timer.setOnClickListener {
            play.visibility = View.GONE
            time.visibility = View.VISIBLE
            viewModel.sprintProgress.value = TrainingProgress.Preparing
        }

        sprintToolbarBack.setOnClickListener {
            handleBack()
        }


        sprint_incorrect.setOnClickListener {
            if (viewModel.answers.value?.get(
                    viewModel.currentAnswer.value ?: -1
                )?.isCorrect == false
            ) {
                animateQuestionResult(true)
                viewModel.incrementCorrectAnswer()
            } else {
                animateQuestionResult(false)
            }
        }

        sprint_correct.setOnClickListener {
            if (viewModel.answers.value?.get(
                    viewModel.currentAnswer.value ?: -1
                )?.isCorrect == true
            ) {
                animateQuestionResult(true)
                viewModel.incrementCorrectAnswer()
            } else {
                animateQuestionResult(false)
            }
        }

    }

    private fun lockButtons() {
        sprint_incorrect.isEnabled = false
        sprint_correct.isEnabled = false
    }

    private fun unlockButtons() {
        sprint_incorrect.isEnabled = true
        sprint_correct.isEnabled = true
    }

    override fun onResume() {
        super.onResume()
        if (view == null) {
            return
        }
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showExitDialog()
                true
            } else false
        }
    }

    private val currentAnswerObserver = Observer<Int> {
        unlockButtons()
        sprint_question_title.text = viewModel.answers.value?.get(it)?.foreign
        sprint_answer_title.text = viewModel.answers.value?.get(it)?.arabic
    }

    private val correctAnswerObserver = Observer<Int> {
        sprint_correct_count.text = getString(R.string.correct_answer).plus(" $it")
    }

    private val dataObserver = Observer<List<GameData>> {

        if (it.isNotEmpty()) {
            viewModel.initQuestions()
        } else {
            alertGameWordsEmpty()
        }
    }

    private val randomDataObserver = Observer<List<Word>> {
        viewModel.getSprintData()
    }

    private val sprintProgressObserver = Observer<TrainingProgress> {
        when (it) {
            TrainingProgress.Preparing -> {
                countDownTimerStart.start()
            }
            TrainingProgress.Started -> {
                timer?.let {
                    timer.visibility = View.GONE
                }
                sprint.visibility = View.VISIBLE
                viewModel.initSprint()
                countDownTimerSprint.startTimer()
            }
            TrainingProgress.Finished -> {
                timerObjectSprint.pauseTimer()
                showResultDialog()
            }
        }
    }

    private val timerObjectStart = object : CountDownTimer(4000, 1000) {

        override fun onFinish() {
            viewModel.sprintProgress.value = TrainingProgress.Started
        }

        override fun onTick(millisUntilFinished: Long) {
            time?.let {
                it.setText((millisUntilFinished.toInt() / 1000).toString())
            }
        }
    }

    private val timerObjectSprint = object : Hourglass(30000, 1000) {
        override fun onTimerTick(timeRemaining: Long) {
            sprint_timer?.let {
                it.setText((timeRemaining.toInt() / 1000).toString().plus("c"))
            }
        }

        override fun onTimerFinish() {
            viewModel.sprintProgress.value = TrainingProgress.Finished
        }
    }

    private fun animateQuestionResult(isCorrect: Boolean) {
        lockButtons()
        sprint_state_image.visibility = View.VISIBLE
        if (isCorrect) {
            sprint_state_image.setImageDrawable(resources.getDrawable(R.drawable.ic_tick))
        } else {
            sprint_state_image.setImageDrawable(resources.getDrawable(R.drawable.ic_wrong))
        }
        sprint_state_image.startAnimation(alphaAnimation)
    }

    private val animationObjectListener = object : AnimationListener {
        override fun onAnimationStart(animation: Animation) {}
        override fun onAnimationEnd(animation: Animation) {
            sprint_state_image.visibility = View.GONE
            viewModel.switchNextAnswer()
        }

        override fun onAnimationRepeat(animation: Animation) {}
    }

    private fun handleBack() {
        when (viewModel.sprintProgress.value) {
            TrainingProgress.Default -> {
                activity?.onBackPressed()
            }
            TrainingProgress.Preparing -> {
                countDownTimerStart.cancel()
                activity?.onBackPressed()
            }
            TrainingProgress.Started -> {
                countDownTimerSprint.pauseTimer()
                showExitDialog()
            }
            TrainingProgress.Finished -> {
                if (countDownTimerSprint.isPaused) {
                    countDownTimerSprint.stopTimer()
                    activity?.onBackPressed()
                }
            }
        }
    }

    private fun alertGameWordsEmpty() {

        activity?.createCustomDialog(titleText = getString(R.string.trainings),
            messageText = getString(R.string.sprint_words_empty),
            dismissCallback = {
                activity?.onBackPressed()
            })

    }

    private fun showResultDialog() {
        activity?.createCustomDialog(titleText = getString(R.string.trainings),
            messageText = getString(
                R.string.your_test_result,
                viewModel.answers.value?.size.toString(),
                viewModel.correctAnswer.value ?: "0"
            ),
            dismissCallback = {
                activity?.onBackPressed()
            })
    }


    private fun showExitDialog() {
        context?.alertWithActions(
            title = getString(R.string.attention),
            message = getString(R.string.sure_to_cancel_training),
            positiveButtonCallback = {
                countDownTimerSprint.stopTimer()
                activity?.onBackPressed()
            },
            cancelable = false,
            negativeButtonCallback = {
                countDownTimerSprint.resumeTimer()
            })
    }

}