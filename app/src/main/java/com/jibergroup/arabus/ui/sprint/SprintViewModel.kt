package com.jibergroup.arabus.ui.sprint

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.repositories.WordRepository
import com.jibergroup.arabus.ui.training_words.GameData
import kotlinx.coroutines.launch
import kotlin.random.Random

class SprintViewModel(private val repository: WordRepository) : ViewModel() {

    val correctAnswer = MutableLiveData<Int>()
    val currentAnswer = MutableLiveData<Int>()
    val sprintProgress = MutableLiveData<TrainingProgress>()
    val answers = MutableLiveData<MutableList<QuestionItem>>()
    val data = MutableLiveData<List<GameData>>()
    val answerRandom = MutableLiveData<List<Word>>()


    init {
        getRandomData()
        sprintProgress.value = TrainingProgress.Default
    }

    fun initSprint() {
        currentAnswer.value = 0
        correctAnswer.value = 0
    }

    fun getRandomData() {
        viewModelScope.launch {
            answerRandom.value = repository.loadRandomAnswer()
        }
    }

    fun incrementCorrectAnswer() {
        correctAnswer.value = correctAnswer.value?.plus(1)
    }

    fun getSprintData() {
        viewModelScope.launch {
            data.value = repository.onLoadTrainingWords().filter { it.sprint == true }
        }
    }

    fun initQuestions() {

        val questions = mutableListOf<QuestionItem>()

        data.value?.forEachIndexed { index, gameData ->
            if (Random.nextBoolean()) {
                questions.add(
                    QuestionItem(
                        arabic = gameData.arabic,
                        foreign = gameData.text,
                        isCorrect = true
                    )
                )
            } else {
                questions.add(
                    QuestionItem(
                        arabic = gameData.arabic,
                        foreign = answerRandom.value?.get(Random.nextInt(0, 999))?.text,
                        isCorrect = false
                    )
                )
            }
        }


        answers.value = questions
    }


    fun switchNextAnswer() {
        if (currentAnswer.value?.compareTo(answers.value?.size ?: -1) == -1) {
            if (currentAnswer.value?.compareTo(answers.value?.size?.minus(1) ?: -1) == 0) {
                sprintProgress.value = TrainingProgress.Finished
            } else {
                currentAnswer.value = currentAnswer.value?.plus(1)
            }
        }
    }


}

data class QuestionItem(
    val arabic: String? = null,
    val foreign: String? = null,
    val isCorrect: Boolean? = null
)

enum class TrainingProgress {
    Default, Preparing, Started, Finished
}