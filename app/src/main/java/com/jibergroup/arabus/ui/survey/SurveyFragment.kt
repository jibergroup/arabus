package com.jibergroup.arabus.ui.survey

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.ui.global.base.alertWithActions
import com.jibergroup.arabus.ui.global.base.createCustomDialog
import com.jibergroup.arabus.ui.sprint.TrainingProgress
import com.jibergroup.arabus.ui.training.TRAIN_ITEM_KEY
import com.jibergroup.arabus.ui.training.TrainingItem
import kotlinx.android.synthetic.main.survey_fragment.*
import kz.jibergroup.studyinn.presentation.course_test.Answer
import kz.jibergroup.studyinn.presentation.course_test.Quiz
import kz.jibergroup.studyinn.presentation.course_test.QuizResult
import kz.jibergroup.studyinn.presentation.course_test.SurveyViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class SurveyFragment : Fragment(R.layout.survey_fragment) {


    var answersAdapter = AnswersAdapter()
    var squareAdapter = SquareGridAdapter()
    val viewModel: SurveyViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        arguments?.getParcelable<TrainingItem>(TRAIN_ITEM_KEY)?.let {
            surveyToolbarTitle.text = it.title
            viewModel.trainItem.value = it
        }

        viewModel.action.observe(viewLifecycleOwner, actionObserver)
        viewModel.trainItem.observe(viewLifecycleOwner, trainItemObserver)
        viewModel.surveyProgress.observe(viewLifecycleOwner, progressObserver)
        viewModel.isDataReady.observe(viewLifecycleOwner, isDataReadyObserver)
        viewModel.answerRandom.observe(viewLifecycleOwner, randomDataObserver)
        initQuestions()

        surveyToolbarBack.setOnClickListener {
            backAction()
        }

    }


    override fun onResume() {
        super.onResume()
        if (view == null) {
            return
        }
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showExitDialog()
                true
            } else false
        }
    }



    private val trainItemObserver = Observer<TrainingItem> {
        viewModel.getRandomData()
    }

    private val randomDataObserver = Observer<List<Word>> {
        viewModel.getGameData()
    }


    private val isDataReadyObserver = Observer<Boolean> {
        if(it){
            viewModel.surveyProgress.value = TrainingProgress.Started
        }
    }

    private fun startTest() {

        if(viewModel.questions.isNotEmpty()){
            viewModel.getSquareList(viewModel.questions)

            setSquareImage(viewModel.squareList)
            getQuizPosition(
                viewModel.quizPos,
                viewModel.questions
            )
        }else{
            alertGameWordsEmpty()
        }

    }

    private fun alertGameWordsEmpty(){
        activity?.createCustomDialog(titleText = getString(R.string.trainings),
            messageText = getString(R.string.sprint_words_empty),
            dismissCallback = {
                onCompleteTest()
            })
    }

    private val actionObserver = Observer<SurveyViewModel.SurveyAction> {
        when (it) {
            SurveyViewModel.SurveyAction.FinishResult -> {
                getFinishResult()
            }
            SurveyViewModel.SurveyAction.CompleteTest -> {
                onCompleteTest()
            }
            SurveyViewModel.SurveyAction.QuizResult -> {
                showResultDialog(viewModel.quizResult)
            }
            SurveyViewModel.SurveyAction.UserAnswerResult -> {
                getUserAnswerResult(++viewModel.quizPos, viewModel.questions)
            }
            else -> {
            }
        }
    }

    private val progressObserver = Observer<TrainingProgress> {
        when (it) {
            TrainingProgress.Started -> {
                startTest()
            }else ->{
            //do nothing
        }
        }
    }

    private fun initQuestions() {
        recyclerAnswersAll.layoutManager = LinearLayoutManager(activity)
        answersAdapter.addContext(activity as AppCompatActivity)
        answersAdapter.setOnClickCheckBox(itemOnClickListener)
        recyclerAnswersAll.adapter = answersAdapter
        recyclerAnswersAll.isNestedScrollingEnabled = false
    }


    private fun getQuizPosition(position: Int, quizList: MutableList<Quiz>) {

        txtQuizTextAll.text = quizList[position].question
        squareAdapter.setCurrentPos(position)
        txtCurrentQuizPosAll.text = getString(R.string.survey_result, quizList.size.toString(),(position + 1).toString())
        answersAdapter.setDataList(quizList[position].answerList)

    }

    private val itemOnClickListener = CompoundButton.OnCheckedChangeListener { view, isChecked ->
        if (isChecked) {
            val ans = view.tag as Answer

            viewModel.getUserAnswer(ans)
        }
    }

    fun getUserAnswerResult(position: Int, quizList: MutableList<Quiz>) {

        val handler2 = Handler()
        handler2.postDelayed({
            startAnimation()
        }, 500)


        val handler = Handler()
        handler.postDelayed({
            answersAdapter.setIsEnabled(true)
            getQuizPosition(position, quizList)
            endAnimation()
        }, 750)

        answersAdapter.setIsEnabled(false)
        answersAdapter.notifyDataSetChanged()
        squareAdapter.notifyDataSetChanged()
    }

    fun getFinishResult() {

        val handler2 = Handler()
        handler2.postDelayed({
        }, 500)

        val handler = Handler()
        handler.postDelayed({
            answersAdapter.setIsEnabled(true)
            squareAdapter.setCurrentPos(100)
            viewModel.setEndResult()
        }, 750)

        answersAdapter.setIsEnabled(false)
        answersAdapter.notifyDataSetChanged()
        squareAdapter.notifyDataSetChanged()
    }

    fun setSquareImage(squareList: MutableList<Int>) {

        recyclerSquareAll.layoutManager =
            LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        recyclerSquareAll.setHasFixedSize(true)
        activity?.let { squareAdapter.addContext(it) }
        squareAdapter.setDataList(squareList)
        recyclerSquareAll.adapter = squareAdapter

    }

    private fun startAnimation() {

        txtCurrentQuizPosAll.animate().scaleX(1.3f).duration = 150
        txtCurrentQuizPosAll.animate().scaleY(1.3f).duration = 150
        txtQuizTextAll.animate().alpha(0.0f).duration = 250
        recyclerAnswersAll.animate().alpha(0.0f).duration = 250
    }

    private fun endAnimation() {

        txtCurrentQuizPosAll.animate().scaleX(1.0f).duration = 150
        txtCurrentQuizPosAll.animate().scaleY(1.0f).duration = 150
        txtQuizTextAll.animate().alpha(1.0f).duration = 250
        recyclerAnswersAll.animate().alpha(1.0f).duration = 250
    }


    override fun onPause() {
        super.onPause()
        txtCurrentQuizPosAll.clearAnimation()
        txtCurrentQuizPosAll.clearAnimation()
        txtQuizTextAll.clearAnimation()
        recyclerAnswersAll.clearAnimation()
    }


    fun onCompleteTest() {
        activity?.onBackPressed()
    }


    private fun showResultDialog(quizResult: QuizResult) {
        activity?.createCustomDialog(titleText = getString(R.string.trainings),
        messageText = getString(R.string.your_test_result, quizResult.max_answers, quizResult.user_answers),
        dismissCallback = {
            onCompleteTest()
        })
    }

    private fun backAction() {
        when (viewModel.surveyProgress.value) {
            TrainingProgress.Started -> {
                showExitDialog()
            }
            else -> {
                onCompleteTest()
            }
        }
    }

    private fun showExitDialog(){
        context?.alertWithActions(
            title = getString(R.string.attention),
            message = getString(R.string.sure_to_cancel_training),
            positiveButtonCallback = {
                onCompleteTest()
            },
            cancelable = false,
            negativeButtonCallback = {
            }, negativeText = getString(R.string.cancel)
        )
    }

}
