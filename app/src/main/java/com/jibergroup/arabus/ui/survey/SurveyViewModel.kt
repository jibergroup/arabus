package kz.jibergroup.studyinn.presentation.course_test

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.db.entities.Word
import com.jibergroup.arabus.data.repositories.WordRepository
import com.jibergroup.arabus.ui.sprint.TrainingProgress
import com.jibergroup.arabus.ui.training.TrainingItem
import com.jibergroup.arabus.ui.training_words.GameData
import com.jibergroup.arabus.ui.training_words.GameModelMapperSurveyArabic
import com.jibergroup.arabus.ui.training_words.GameModelMapperSurveyForeign
import com.jibergroup.arabus.ui.training_words_action.TrainingType
import kotlinx.coroutines.launch
import kotlin.random.Random

class SurveyViewModel(private val repository: WordRepository) : ViewModel() {

    val quizResult: QuizResult = QuizResult()
    var questions = mutableListOf<Quiz>()
    var squareList = mutableListOf<Int>()

    var quizPos: Int = 0
    var userPoints: Int = 0
    var userAnswers: Int = 0
    var maxPoints: Int = 0
    var maxAnswers: Int = 0

    val action = MutableLiveData<SurveyAction>()
    val trainItem = MutableLiveData<TrainingItem>()
    val surveyProgress = MutableLiveData<TrainingProgress>()

    val answerRandom = MutableLiveData<List<Word>>()
    val isDataReady = MutableLiveData<Boolean>()

    fun getRandomData() {
        viewModelScope.launch {
            answerRandom.value = repository.loadRandomAnswer()
        }
    }

    fun getGameData() {
        when (trainItem.value?.trainingType) {
            TrainingType.ForeignToArabic -> {
                getForeignDictionaryWords()
            }
            TrainingType.ArabicToForeign -> {
                getArabicDictionaryWords()
            }
        }
    }

    fun getArabicDictionaryWords() {
        viewModelScope.launch {
            val mapper = GameModelMapperSurveyArabic()
            val data = repository.onLoadTrainingWords().filter { it.arabicGame == true }
            questions = data.map { mapper.transform(it) }.toMutableList()
            getAnswerListRandom()
            isDataReady.value = true
        }
    }

    fun getForeignDictionaryWords() {
        viewModelScope.launch {
            val mapper = GameModelMapperSurveyForeign()
            val data = repository.onLoadTrainingWords().filter { it.foreignGame == true }
            questions = data.map { mapper.transform(it) }.toMutableList()
            getAnswerListRandom()
            isDataReady.value = true
        }
    }

    private fun getAnswerListRandom() {
        questions.forEach {

            val answerList = mutableListOf<Answer>()

            answerList.add(0, Answer(Random.nextInt(0, 100), it.answer, true))

            for (i in 1..4) {
                answerList.add(
                    i,
                    Answer(
                        Random.nextInt(0, 100),
                        if (trainItem.value?.trainingType == TrainingType.ArabicToForeign)
                            answerRandom.value?.get(Random.nextInt(0, 999))?.text?.replace("-","")
                        else
                            answerRandom.value?.get(Random.nextInt(0, 999))?.arabic,
                        false
                    )
                )
            }

            answerList.shuffle()

            it.answerList = answerList

        }
    }

    fun getUserAnswer(ans: Answer) {

        if (ans.is_correct) {
            ans.user_answer = 1
            squareList[quizPos] = 1

            val points = questions[quizPos].point

            points?.let {
                userPoints += it
            }

            userAnswers = ++userAnswers


        } else {

            questions[quizPos].answerList?.let {

                for (answer in it) {
                    if (answer.is_correct) {
                        answer.user_answer = 1
                    }
                }
            }

            ans.user_answer = -1
            squareList[quizPos] = -1

        }

        if (quizPos < questions.size - 1) {
            action.value = SurveyAction.UserAnswerResult
        } else {
            action.value = SurveyAction.FinishResult
        }

    }

    fun getSquareList(questions: MutableList<Quiz>) {
        val data = squareList
        for (i in 0 until questions.size) {
            data.add(0)
        }

        squareList = data


        maxAnswers = questions.size

        for (quis in questions) {
            quis.point?.let { point ->
                maxPoints += point
            }
        }

    }


    fun setEndResult() {

        quizResult.max_points = maxPoints.toString()
        quizResult.user_points = userPoints.toString()
        quizResult.max_answers = maxAnswers.toString()
        quizResult.user_answers = userAnswers.toString()

        action.value = SurveyAction.QuizResult
    }

    enum class SurveyAction {
        Preparing, Start, FinishResult, CompleteTest, QuizResult, UserAnswerResult
    }

}