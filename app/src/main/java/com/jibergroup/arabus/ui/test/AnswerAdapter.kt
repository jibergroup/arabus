package com.jibergroup.arabus.ui.test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.global.base.BaseViewHolder
import com.jibergroup.arabus.ui.training_words.ItemClickListener
import kotlinx.android.synthetic.main.question_item.view.*

class AnswerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataSet = mutableListOf<AnswerItem>()
    lateinit var listener: ItemClickListener

    fun submitData(menuList: MutableList<AnswerItem>) {
        this.dataSet = menuList
        notifyDataSetChanged()
    }

    fun submitListener(listener: ItemClickListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.question_item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.question_item -> AnswerViewHolder(view)
            else -> AnswerViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AnswerViewHolder -> holder.bind(dataSet[position])
        }
    }

    inner class AnswerViewHolder(itemView: View) : BaseViewHolder<AnswerItem>(itemView) {

        override fun bind(item: AnswerItem) {

            itemView.answerTitle.text = item.title

            when (item.userAnswerType) {
                UserAnswerType.InCorrect -> {
                    itemView.answerCard.setBackground(itemView.context.resources.getDrawable(R.drawable.shape_test_incorrect))
                    itemView.answerTitle.setTextColor(itemView.context.resources.getColor(R.color.white))
                }
                UserAnswerType.Correct -> {
                    itemView.answerCard.setBackground(itemView.context.resources.getDrawable(R.drawable.shape_test_correct))
                    itemView.answerTitle.setTextColor(itemView.context.resources.getColor(R.color.white))
                }
                UserAnswerType.Default -> {
                    itemView.answerCard.setBackground(itemView.context.resources.getDrawable(R.drawable.shape_test_default))
                }
            }

            itemView.setOnClickListener {
                listener.onClickItem(adapterPosition)
            }
        }

    }

}