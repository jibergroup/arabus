package com.jibergroup.arabus.ui.test

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.sprint.TrainingProgress
import com.jibergroup.arabus.ui.training_words.ItemClickListener
import kotlinx.android.synthetic.main.sprint_fragment.*
import kotlinx.android.synthetic.main.test_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TestFragment : Fragment(R.layout.test_fragment),ItemClickListener {

    val viewModel: TestViewModel by viewModel()

    lateinit var adapter: AnswerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initView()
        testToolbarTitle.text = "Қазақша-Арабша"
        testToolbarBack.setOnClickListener {
            activity?.onBackPressed()
        }

        viewModel.questions.observe(activity as AppCompatActivity,questionObserver)
        viewModel.currentQuestion.observe(activity as AppCompatActivity,currentQuestionObserver)
        viewModel.currentQuestionOrder.observe(activity as AppCompatActivity,currentQuestionObserverOrder)
        viewModel.testProgress.observe(activity as AppCompatActivity,testProgressObserver)


        testNextBtn.setOnClickListener {
            viewModel.switchNextQuestion()
        }

    }

    private val questionObserver = Observer<MutableList<QuestionItem>>{
        viewModel.initTest()
    }

    private val currentQuestionObserver = Observer<QuestionItem>{
        adapter.submitData(it.answers ?: mutableListOf())
        testWord.text = it.title
    }

    private val currentQuestionObserverOrder = Observer<Int>{
        viewModel.currentQuestion.value = viewModel.questions.value?.get(it)
    }


    private fun initView(){

        adapter = AnswerAdapter()
        adapter.submitListener(this)

        testAnswerRecycler.layoutManager = LinearLayoutManager(activity)
        testAnswerRecycler.adapter = adapter
    }


    private val testProgressObserver = Observer<TrainingProgress> {
        when (it) {
            TrainingProgress.Preparing -> {

            }
            TrainingProgress.Started -> {

            }
            TrainingProgress.Finished -> {

            }
        }
    }



    override fun onClickItem(position: Int) {
        viewModel.selectAnswer(position)
    }
}