package com.jibergroup.arabus.ui.test

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jibergroup.arabus.ui.sprint.TrainingProgress

class TestViewModel : ViewModel() {

    val questions = MutableLiveData<MutableList<QuestionItem>>()
    val currentQuestion = MutableLiveData<QuestionItem>()
    val currentQuestionOrder = MutableLiveData<Int>()
    val correctAnswerCount = MutableLiveData<Int>()
    val testProgress = MutableLiveData<TrainingProgress>()

    init {
        initData()
    }

    fun initTest(){
        currentQuestionOrder.value = 0
    }


    fun switchNextQuestion(){

        if(currentQuestionOrder.value?.compareTo(questions.value?.size ?: -1) == -1){
            if(currentQuestionOrder.value?.compareTo(questions.value?.size?.minus(1) ?: -1) == 0){
                testProgress.value = TrainingProgress.Finished
            }else{
                currentQuestionOrder.value = currentQuestionOrder.value?.plus(1)
            }
        }

    }

    fun incrementCorrectAnswer(){
        correctAnswerCount.value = correctAnswerCount.value?.plus(1)
    }

    fun selectAnswer(position : Int){

        if(currentQuestion.value?.answers?.get(position)?.isCorrect ?: false){

            incrementCorrectAnswer()
            val newCurrent = currentQuestion
            newCurrent.value?.answers?.get(position)?.userAnswerType = UserAnswerType.Correct
            currentQuestion.value = newCurrent.value

        }else{

            val newCurrent = currentQuestion
            newCurrent.value?.answers?.get(position)?.userAnswerType = UserAnswerType.InCorrect

            var correctIndex = -1
            newCurrent.value?.answers?.forEachIndexed { index, answerItem ->
                if(answerItem.isCorrect ?: false){
                    correctIndex = index
                }
            }
            newCurrent.value?.answers?.get(correctIndex)?.userAnswerType = UserAnswerType.Correct

            currentQuestion.value = newCurrent.value

        }

    }


    private fun initData(){

        val questionList = mutableListOf<QuestionItem>()


        val answers = mutableListOf<AnswerItem>()
        answers.add(AnswerItem(id = 0,title = "Арыстан",isCorrect = true))
        answers.add(AnswerItem(id = 0,title = "Көру",isCorrect = false))
        answers.add(AnswerItem(id = 0,title = "Мысық",isCorrect = false))
        answers.add(AnswerItem(id = 0,title = "Оқу",isCorrect = false))

        questionList.add(QuestionItem(id = 0,title = "طفل ليو",answers = answers))
        questionList.add(QuestionItem(id = 0,title = "أنا لا أعرف العربية ليو",answers = answers))
        questionList.add(QuestionItem(id = 0,title = "معذرة ليو",answers = answers))
        questionList.add(QuestionItem(id = 0,title = "شغل",answers = answers))


        questions.value = questionList


    }

}

data class QuestionItem(
    val id : Long? = null,
    val title : String? = null,
    val answers : MutableList<AnswerItem>? = null
)

data class AnswerItem(
    val id : Long? = null,
    val title : String? = null,
    val isCorrect : Boolean? = null,
    var userAnswerType: UserAnswerType = UserAnswerType.Default
)

enum class UserAnswerType{
    Default, Correct, InCorrect
}
