package com.jibergroup.arabus.ui.training

import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.training_words_action.TrainingType
import kotlinx.android.synthetic.main.fragment_training.*


const val TRAIN_ITEM_KEY = "TRAIN_ITEM_KEY"

class TrainingFragment : Fragment(R.layout.fragment_training) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        trainingToolbarTitle.text = getString(R.string.trainings)
        trainingWordsShowBtn.setOnClickListener {
            findNavController().navigate(R.id.action_trainingFragment_to_trainingWordsFragment)
        }

        cardSprint.setOnClickListener {
            findNavController().navigate(R.id.action_trainingFragment_to_sprintFragment)
        }

        cardArabic.setOnClickListener {
            findNavController().navigate(
                R.id.action_trainingFragment_to_surveyFragment,
                getTrainItem(TrainingType.ArabicToForeign)
            )
        }

        cardKazakh.setOnClickListener {
            findNavController().navigate(
                R.id.action_trainingFragment_to_surveyFragment,
                getTrainItem(TrainingType.ForeignToArabic)
            )
        }

        cardConstructor.setOnClickListener {
            findNavController().navigate(
                R.id.action_trainingFragment_to_constructorFragment,
                getTrainItem(TrainingType.Constructor)
            )
        }
    }

    private fun getTrainItem(trainingType: TrainingType): Bundle {
        val data = Bundle()

        val trainItem = TrainingItem()
        trainItem.trainingType = trainingType

        when (trainingType) {

            TrainingType.ArabicToForeign -> {
                val title =
                    SpannableString("${getString(R.string.arabic_first)} \u2192 ${getString(R.string.kazakh)}")
                title.setSpan(RelativeSizeSpan(2f), 1, 2, 0)
                trainItem.title = title.toString()
            }
            TrainingType.ForeignToArabic -> {
                val title =
                    SpannableString("${getString(R.string.foreign_first)} \u2192 ${getString(R.string.foreign_second)}")
                title.setSpan(RelativeSizeSpan(2f), 1, 2, 0)
                trainItem.title = title.toString()
            }
            TrainingType.Sprint -> {
                trainItem.title = getString(R.string.sprint)
            }
            TrainingType.Constructor -> {
                trainItem.title = getString(R.string.constructor)
            }

        }


        data.putParcelable(TRAIN_ITEM_KEY, trainItem)

        return data
    }

}