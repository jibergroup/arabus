package com.jibergroup.arabus.ui.training

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jibergroup.arabus.ui.training_words_action.TrainingType


data class TrainingItem(
    var srcImage: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var fromLanguage: String? = null,
    var toLanguage: String? = null,
    var trainingType: TrainingType? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TrainingType.valueOf(parcel.readString() ?: "")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(srcImage)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(fromLanguage)
        parcel.writeString(toLanguage)
        parcel.writeString(trainingType?.name.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TrainingItem> {
        override fun createFromParcel(parcel: Parcel): TrainingItem {
            return TrainingItem(parcel)
        }

        override fun newArray(size: Int): Array<TrainingItem?> {
            return arrayOfNulls(size)
        }
    }
}

interface TrainingClickListener {
    fun onTrainingClick(trainingType: TrainingType)
}