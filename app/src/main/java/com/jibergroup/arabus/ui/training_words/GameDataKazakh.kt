package com.jibergroup.arabus.ui.training_words

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import com.jibergroup.arabus.data.db.entities.KazakhGames
import com.jibergroup.arabus.data.db.entities.KazakhWord
import com.jibergroup.arabus.data.db.entities.RussianGames
import com.jibergroup.arabus.data.db.entities.RussianWord
import com.jibergroup.arabus.data.mapper.BaseMapper
import kz.jibergroup.studyinn.presentation.course_test.Quiz

class GameDataKazakh {
    @Embedded
    lateinit var kazakhWord: KazakhWord

    @Relation(parentColumn = "id", entityColumn = "word_id")
    var games: List<KazakhGames>? = null

    fun getKazakhGame() : KazakhGames?{
        return games?.get(0)
    }
}

class GameDataRussian {
    @Embedded
    lateinit var russianWord: RussianWord

    @Relation(parentColumn = "id", entityColumn = "word_id")
    var games: List<RussianGames>? = null

    fun getRussianGame() : RussianGames?{
        return games?.get(0)
    }
}

class GameData(
    val id: Int?,
    val text: String?,
    val arabic: String?,
    val arabicSecond: String?,
    val description: String?,
    var isInGame: Boolean?,
    var constructor: Boolean?,
    var sprint: Boolean?,
    var arabicGame: Boolean?,
    var foreignGame: Boolean?
    ) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(text)
        parcel.writeString(arabic)
        parcel.writeString(arabicSecond)
        parcel.writeString(description)
        parcel.writeValue(isInGame)
        parcel.writeValue(constructor)
        parcel.writeValue(sprint)
        parcel.writeValue(arabicGame)
        parcel.writeValue(foreignGame)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GameData> {
        override fun createFromParcel(parcel: Parcel): GameData {
            return GameData(parcel)
        }

        override fun newArray(size: Int): Array<GameData?> {
            return arrayOfNulls(size)
        }
    }
}

class GameModelMapperKazakh : BaseMapper<GameDataKazakh, GameData> {
    override fun transform(type: GameDataKazakh): GameData {
        return GameData(
            id = type.kazakhWord.id,
            text = type.kazakhWord.kazakh,
            arabic = type.kazakhWord.arabic,
            arabicSecond = type.kazakhWord.arabicSecond,
            description = type.kazakhWord.description,
            isInGame = type.kazakhWord.isInGame,
            constructor = type.getKazakhGame()?.constructor ?: false,
            sprint = type.getKazakhGame()?.sprint ?: false,
            arabicGame = type.getKazakhGame()?.arabicGame ?: false,
            foreignGame = type.getKazakhGame()?.foreignGame ?: false
        )
    }

    override fun transformToDatabase(type: GameData): GameDataKazakh {
        return GameDataKazakh()
    }
}

class GameModelMapperRussian : BaseMapper<GameDataRussian, GameData> {
    override fun transform(type: GameDataRussian): GameData {
        return GameData(
            id = type.russianWord.id,
            text = type.russianWord.russian,
            arabic = type.russianWord.arabic,
            arabicSecond = type.russianWord.arabicSecond,
            description = type.russianWord.description,
            isInGame = type.russianWord.isInGame,
            constructor = type.getRussianGame()?.constructor ?: false,
            sprint = type.getRussianGame()?.sprint ?: false,
            arabicGame = type.getRussianGame()?.arabicGame ?: false,
            foreignGame = type.getRussianGame()?.foreignGame ?: false
        )
    }

    override fun transformToDatabase(type: GameData): GameDataRussian {
        return GameDataRussian()
    }
}

class GameModelMapperSurveyArabic: BaseMapper<GameData, Quiz> {
    override fun transform(type: GameData): Quiz {
        return Quiz(
            id = type.id,
            question = type.arabic,
            answer = type.text,
            point = 1,
            answerList = mutableListOf()

        )
    }

    override fun transformToDatabase(quiz: Quiz): GameData {
        return GameData(-1,"","","","",false,false,false,false,false)
    }
}

class GameModelMapperSurveyForeign: BaseMapper<GameData, Quiz> {
    override fun transform(type: GameData): Quiz {
        return Quiz(
            id = type.id,
            question = type.text,
            answer = type.arabic,
            point = 1,
            answerList = mutableListOf()

        )
    }

    override fun transformToDatabase(quiz: Quiz): GameData {
        return GameData(-1,"","","","",false,false,false,false,false)
    }
}