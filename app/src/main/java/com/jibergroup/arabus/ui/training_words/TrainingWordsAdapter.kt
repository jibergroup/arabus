package com.jibergroup.arabus.ui.training_words

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeAdapter
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.global.base.BaseViewHolder
import kotlinx.android.synthetic.main.train_word_item.view.*


class TrainingWordsAdapter(dataSet: List<GameData> = emptyList()) :
    DragDropSwipeAdapter<GameData, TrainingWordsAdapter.WordsViewHolder>() {

    lateinit var listener: GameWordsItemClickListener
    fun submitData(menuList: List<GameData>) {
        this.dataSet = menuList
        notifyDataSetChanged()
    }

    fun submitListener(listener: GameWordsItemClickListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.train_word_item
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun getViewHolder(itemView: View): WordsViewHolder {
        return WordsViewHolder(itemView)
    }


    override fun getViewToTouchToStartDraggingItem(
        item: GameData,
        viewHolder: WordsViewHolder,
        position: Int
    ): View? {
        return viewHolder.trainWordArabic
    }

    override fun onBindViewHolder(item: GameData, viewHolder: WordsViewHolder, position: Int) {
        viewHolder.trainWordKazakh.text = item.text
        viewHolder.trainWordArabic.text = item.arabic
        viewHolder.trainWordArabicSecond.text = item.arabicSecond
        viewHolder.layout.setOnClickListener {
            listener.onClickItem(viewHolder.adapterPosition)
        }

    }

    inner class WordsViewHolder(itemView: View) : DragDropSwipeAdapter.ViewHolder(itemView) {

        val trainWordKazakh = itemView.findViewById(R.id.trainWordKazakh) as TextView
        val trainWordArabic = itemView.findViewById(R.id.trainWordArabic) as TextView
        val trainWordArabicSecond = itemView.findViewById(R.id.trainWordArabicSecond) as TextView
        val layout = itemView.findViewById(R.id.swipeRevealLayout) as LinearLayout

//        override fun bind(item: GameData) {
//
//            itemView.measure(
//                View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
//
//            itemView.trainWordKazakh.text = item.text
//            itemView.trainWordArabic.text = item.arabic
//            itemView.trainWordArabicSecond.text = item.arabicSecond
//
//            itemView.delete_layout.layoutParams = RelativeLayout.LayoutParams(
//                250,
//                itemView.measuredHeight
//            )
//
//            itemView.front_layout.setOnClickListener {
//                listener.onClickItem(adapterPosition)
//            }
//
//            itemView.delete_layout.setOnClickListener {
//                listener.onDeleteItem(adapterPosition)
//            }
//
//            itemView.delete.setOnClickListener {
//                listener.onDeleteItem(adapterPosition)
//            }
//        }

    }

}