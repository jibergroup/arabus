package com.jibergroup.arabus.ui.training_words

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeRecyclerView
import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnItemSwipeListener
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.db.entities.KazakhGames
import com.jibergroup.arabus.ui.global.base.alertWithActions
import com.jibergroup.arabus.ui.training_words_action.WORD_KEY
import kotlinx.android.synthetic.main.training_words_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TrainingWordsFragment : Fragment(R.layout.training_words_fragment),GameWordsItemClickListener {

    val viewModel: TrainingWordsViewModel by viewModel()
    lateinit var adapter: TrainingWordsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        trainingWordsToolbarTitle.text = getString(R.string.word_list)
        initView()

        trainingWordsToolbarBack.setOnClickListener {
            activity?.onBackPressed()
        }

        trainingWordsClear.setOnClickListener {
            showClearWordsDialog()
        }

        viewModel.trainingWords.observe(activity as AppCompatActivity,wordsObserver)

    }

    override fun onResume() {
        super.onResume()
        viewModel.initData()
    }

    private val wordsObserver = Observer<List<GameData>>{
        if(it.isEmpty()){
            showEmptyLayout()
        }else{
            trainingWordsClear.visibility = View.VISIBLE
            adapter.submitData(viewModel.trainingWords.value ?: listOf())
        }
    }

    private fun showEmptyLayout(){
        trainingWordsRecycler.visibility = View.GONE
        empty_layout.visibility = View.VISIBLE
        trainingWordsClear.visibility = View.GONE
    }

    private fun showClearWordsDialog(){
        context?.alertWithActions(
            title = getString(R.string.attention),
            message = getString(R.string.clear_game_word),
            positiveButtonCallback = {
                viewModel.clearGameWords()
            },
            cancelable = false,
            negativeButtonCallback = {})
    }

    private fun initView(){

        adapter = TrainingWordsAdapter()
        adapter.submitData(viewModel.trainingWords.value ?: listOf())
        adapter.submitListener(this)

        trainingWordsRecycler.adapter = adapter
        trainingWordsRecycler.layoutManager = LinearLayoutManager(activity)
        trainingWordsRecycler.swipeListener = onItemSwipeListener

    }

    override fun onClickItem(position: Int) {
        findNavController().navigate(R.id.action_trainingWordsFragment_to_trainingWordsActionFragment,getWordItem(position))
    }

    private val onItemSwipeListener = object : OnItemSwipeListener<GameData> {
        override fun onItemSwiped(position: Int, direction: OnItemSwipeListener.SwipeDirection, item: GameData): Boolean {
            viewModel.deleteData(position)
            return true
        }
    }

    override fun onDeleteItem(position: Int) {
        viewModel.deleteData(position)
    }

    private fun getWordItem(position: Int) : Bundle{
        return Bundle().apply {
            putParcelable(WORD_KEY, viewModel.trainingWords.value?.get(position))
        }
    }

}