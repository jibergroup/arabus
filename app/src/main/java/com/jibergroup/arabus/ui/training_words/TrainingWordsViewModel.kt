package com.jibergroup.arabus.ui.training_words

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.data.repositories.WordRepository
import kotlinx.coroutines.launch

class TrainingWordsViewModel(
    private val repository: WordRepository
) : ViewModel() {

    val trainingWords = MutableLiveData<List<GameData>>()


    fun initData(){
        viewModelScope.launch {
            trainingWords.value = repository.onLoadTrainingWords()
        }
    }

    fun deleteData(position: Int){
        val id = trainingWords.value?.get(position)?.id ?: -1
        viewModelScope.launch {
            repository.updateStateToSprint(id,false)
            initData()
        }
    }

    fun clearGameWords(){
        viewModelScope.launch {
            repository.clearGameWords(trainingWords.value ?: mutableListOf())
            initData()
        }
    }

}

interface ItemClickListener{
    fun onClickItem(position : Int)
}

interface GameWordsItemClickListener{
    fun onClickItem(position : Int)
    fun onDeleteItem(position : Int)
}