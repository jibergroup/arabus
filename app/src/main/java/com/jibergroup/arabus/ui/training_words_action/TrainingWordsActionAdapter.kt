package com.jibergroup.arabus.ui.training_words_action

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.global.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_training_action.view.*

class TrainingWordsActionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataSet = mutableListOf<GameItem>()
    lateinit var listener: ActionItemCheckedListener

    fun submitData(menuList: MutableList<GameItem>) {
        this.dataSet = menuList
        notifyDataSetChanged()
    }

    fun submitListener(listener: ActionItemCheckedListener) {
        this.listener = listener
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_training_action
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_training_action -> ActionViewHolder(view)
            else -> ActionViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ActionViewHolder -> holder.bind(dataSet[position])
        }
    }

    inner class ActionViewHolder(itemView: View) : BaseViewHolder<GameItem>(itemView) {

        override fun bind(item: GameItem) {

            itemView.itemTrainingTitle.text = itemView.resources.getString(item.title)
            item.titleSecond?.let {
                itemView.itemTrainingTitle.text = itemView.resources.getString(item.title).plus("-")
                    .plus(itemView.resources.getString(item.titleSecond))
            }
            itemView.itemTrainingAction.isChecked = item.isEnabled

            itemView.itemTrainingAction.setOnCheckedChangeListener { _, isChecked ->
                item.isEnabled = isChecked
                listener.onActionCheck(isChecked, item.trainingType)
            }


        }

    }

}