package com.jibergroup.arabus.ui.training_words_action

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jibergroup.arabus.R
import com.jibergroup.arabus.ui.training_words.GameData
import kotlinx.android.synthetic.main.training_words_action_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

const val WORD_KEY = "WORD_KEY"

class TrainingWordsActionFragment : Fragment(R.layout.training_words_action_fragment),ActionItemCheckedListener {

    val viewModel: TrainingWordsActionViewModel by viewModel()
    lateinit var adapter: TrainingWordsActionAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments?.getParcelable<GameData>(WORD_KEY)?.let {
            viewModel.word.value = it
        }

        viewModel.trainings.setObserver(activity as AppCompatActivity,trainingsObserver)
        viewModel.word.observe(activity as AppCompatActivity,wordObserver)
        viewModel.changeGameType.observe(activity as AppCompatActivity,changeGameTypeObserver)

    }

    private val trainingsObserver = Observer<MutableList<GameItem>>{
        initView()
    }

    private val changeGameTypeObserver = Observer<MutableList<Int>>{
        it.forEach {
            trainingWordsActionRecycler.post {
                adapter.notifyItemChanged(it)
            }
        }
    }

    private val wordObserver = Observer<GameData> {
        trainingWordsActionToolbarTitle.text = it.arabic
        viewModel.initTrainingType()
    }

    private fun initView(){

        adapter = TrainingWordsActionAdapter()
        adapter.submitData(viewModel.trainings.value ?: mutableListOf())
        adapter.submitListener(this)

        trainingWordsActionRecycler.adapter = adapter
        trainingWordsActionRecycler.layoutManager = LinearLayoutManager(activity)

        trainingWordsActionToolbarBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onActionCheck(isChecked: Boolean, type: TrainingType) {
       viewModel.updateGameMode(type,isChecked)
    }
}