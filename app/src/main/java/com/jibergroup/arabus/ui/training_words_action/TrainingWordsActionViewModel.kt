package com.jibergroup.arabus.ui.training_words_action

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jibergroup.arabus.R
import com.jibergroup.arabus.data.repositories.WordRepository
import com.jibergroup.arabus.ui.training_words.GameData
import kotlinx.android.synthetic.main.sprint_fragment.*
import kotlinx.coroutines.launch

const val ALL_GAME_INDEX = 0
const val FOREIGN_GAME_INDEX = 1
const val ARABIC_GAME_INDEX = 2
const val SPRINT_GAME_INDEX = 3
const val CONSTRUCTOR_GAME_INDEX = 4

class TrainingWordsActionViewModel(private val repository: WordRepository) : ViewModel() {

    val trainings = SingleLiveEvent<MutableList<GameItem>>()
    val word = MutableLiveData<GameData>()
    val changeGameType = MutableLiveData<MutableList<Int>>()

    fun initTrainingType(){

        val data = mutableListOf<GameItem>()

        data.add(GameItem(title = R.string.all,trainingType = TrainingType.All,isEnabled = isGameItemSprintMode()))
        data.add(GameItem(title = R.string.foreign_first,titleSecond = R.string.foreign_second,trainingType = TrainingType.ForeignToArabic,isEnabled = word.value?.foreignGame ?: false))
        data.add(GameItem(title = R.string.arabic_first,titleSecond = R.string.arabic_second,trainingType = TrainingType.ArabicToForeign,isEnabled = word.value?.arabicGame ?: false))
        data.add(GameItem(title = R.string.sprint,trainingType = TrainingType.Sprint,isEnabled = word.value?.sprint ?: false))
        data.add(GameItem(title = R.string.constructor,trainingType = TrainingType.Constructor,isEnabled = word.value?.constructor ?: false))

        trainings.value = data

    }

    private fun isGameItemSprintMode() : Boolean{
        return (word.value?.sprint ?: false
                && word.value?.constructor ?: false
                && word.value?.arabicGame ?: false
                && word.value?.foreignGame ?: false)
    }

    fun updateGameMode(type: TrainingType, state : Boolean){
        val id = word.value?.id ?: -1
        viewModelScope.launch {
            when (type) {
                TrainingType.All -> {
                    repository.updateGameModeAll(id, state)
                }
                TrainingType.ArabicToForeign -> {
                    repository.updateGameModeArabic(id, state)
                }
                TrainingType.ForeignToArabic -> {
                    repository.updateGameModeForeign(id, state)
                }
                TrainingType.Sprint-> {
                    repository.updateGameModeSprint(id, state)
                }
                TrainingType.Constructor -> {
                    repository.updateGameModeConstructor(id, state)
                }
            }
        }
        updateGameDataLocal(type,state)
    }

    private fun updateGameDataLocal(
        type: TrainingType,
        state: Boolean
    ){

        val changes = mutableListOf<Int>()

        when (type) {
            TrainingType.All -> {
                trainings.value?.forEachIndexed { index, gameItem ->
                    if(gameItem.trainingType != TrainingType.All){
                        gameItem.isEnabled = state
                        changes.add(index)
                    }
                }
            }
            else -> {
                if(checkForChangeAllType(state)){
                    changes.add(ALL_GAME_INDEX)
                }
            }
        }
        changeGameType.value = changes
    }

    private fun checkForChangeAllType(state: Boolean) : Boolean{

        var countPlus = 0
        var countMinus = 0
        trainings.value?.forEach {
            if(it.trainingType != TrainingType.All){
                if(it.isEnabled){
                    countPlus++
                }else{
                    countMinus++
                }
            }
        }

        if(state){
            if(countPlus == 4){
                trainings.value?.get(ALL_GAME_INDEX)?.isEnabled = state
                return true
            }
        }else{
            if(countMinus != 4){
                if(trainings.value?.get(ALL_GAME_INDEX)?.isEnabled == true){
                    trainings.value?.get(ALL_GAME_INDEX)?.isEnabled = state
                    return true
                }

            }
        }

        return false

    }


}

enum class TrainingType{
    Sprint, ArabicToForeign, ForeignToArabic, Constructor, All
}

data class GameItem(
    val title : Int,
    val titleSecond : Int? = null,
    val trainingType: TrainingType,
    var isEnabled : Boolean
)

interface ActionItemCheckedListener{
    fun onActionCheck(isChecked : Boolean, type: TrainingType)
}