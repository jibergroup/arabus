package com.jibergroup.arabus.utils

object Constants{
    const val ARABIC_LETTERS = "ضصثقفغعهخحجشسبلتنمكظطذدزراأإآيىئوؤ"
    const val EASY_ARABIC_LETTERS = "ضصثقفغعهخحجشسبلتنمكظطذدزراااايىىوو"
}
