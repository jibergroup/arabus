package com.jibergroup.arabus.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

fun Intent.createInstaAppIntent(profileId: String, context: Context?){
    this.action = Intent.ACTION_VIEW
    this.data = Uri.parse("http://instagram.com/_u/$profileId")
    this.`package` = "com.instagram.android"
    try {
        context?.startActivity(this)
    } catch (e: ActivityNotFoundException) {
        context?.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://instagram.com/$profileId")
            )
        )
    }
}

fun Intent.createTelegramIntent(url: String, context: Context?){
    this.action = Intent.ACTION_VIEW
    this.data = Uri.parse("https://telegram.me/$url")
    context?.startActivity(this)
}

fun Intent.createBrowseIntent(url: String, context: Context?, tag: String? = "https://"){
    this.action = Intent.ACTION_VIEW
    this.data = Uri.parse(tag + "telegram.me/$url")
    context?.startActivity(this)
}