package com.jibergroup.arabus.utils

import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    @TypeConverter
    fun toDate(timestamp: Long?): Date? {
        return timestamp?.let { Date(it) }
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun calendarFromTimestamp(value: String?): Calendar? {
        if (value == null) {
            return null
        }
        val cal: Calendar = GregorianCalendar()
        cal.timeInMillis = value.toLong() * 1000
        return cal
    }

    @TypeConverter
    fun dateToTimestamp(cal: Calendar?): String? {
        return if (cal == null) {
            null
        } else "" + cal.timeInMillis / 1000
    }
}