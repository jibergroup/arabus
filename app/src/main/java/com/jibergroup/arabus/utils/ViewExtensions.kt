package com.jibergroup.arabus.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.text.Selection
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.ImageButton
import android.widget.TextView
import com.jibergroup.arabus.BuildConfig
import com.jibergroup.arabus.R


fun Context.copyToClipboard(text: String?){
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("label",text)
    clipboard.setPrimaryClip(clip)
}

fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
    val spannableString = SpannableString(this.text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    this.movementMethod = LinkMovementMethod.getInstance()
    this.setText(spannableString, TextView.BufferType.SPANNABLE)
}

fun ImageButton.share(context: Context, arabicWord: String){
    val startIntent = Intent(Intent.ACTION_SEND)
    startIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    startIntent.type = "text/plain"
    startIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
//    val message = arabicWord +
//            "\n\nАрабша - қазақша мобильдік қосымшаны жүктеп алыңыз.\n" +
//            "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
    startIntent.putExtra(Intent.EXTRA_TEXT, arabicWord)
    val shareIntent = Intent.createChooser(startIntent, context.getString(R.string.intent_choser_title))
    shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    context.startActivity(shareIntent)
}

fun ImageButton.changeFavoriteImage(isFavorite: Boolean){
    if(isFavorite){
        this.setImageResource(R.drawable.ic_star_active)
    }
    else{
        this.setImageResource(R.drawable.ic_star_inactive)
    }
}

fun View.setMargins(
    left: Int,
    top: Int,
    right: Int,
    bottom: Int
) {
    if (this.layoutParams is MarginLayoutParams) {
        val p = this.layoutParams as MarginLayoutParams
        p.setMargins(left, top, right, bottom)
        this.requestLayout()
    }
}

